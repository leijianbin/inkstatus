<?php

class Cmsmart_Productvote_Block_Adminhtml_Productvote_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'productvote';
        $this->_controller = 'adminhtml_productvote';
        
        $this->_updateButton('save', 'label', Mage::helper('productvote')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('productvote')->__('Delete'));	
    }

    public function getHeaderText()
    {
        if( Mage::registry('productvote_data') && Mage::registry('productvote_data')->getId() ) {
            return Mage::helper('productvote')->__("Edit", $this->htmlEscape(Mage::registry('productvote_data')->getTitle()));
        } else {
            return Mage::helper('productvote')->__('Add');
        }
    }
}