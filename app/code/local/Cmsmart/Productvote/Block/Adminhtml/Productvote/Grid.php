<?php

class Cmsmart_Productvote_Block_Adminhtml_Productvote_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('productvoteGrid');
      $this->setDefaultSort('id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
     
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('productvote/productvote')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('id', array(
          'header'    => Mage::helper('productvote')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'id',
      ));

      $this->addColumn('product_id', array(
          'header'    => Mage::helper('productvote')->__('Product ID'),
          'align'     =>'left',
          'index'     => 'product_id',
      ));
       $this->addColumn('product_name', array(
          'header'    => Mage::helper('productvote')->__('Product Name'),
          'align'     =>'left',
          'index'     => 'product_name',
      ));
        $this->addColumn('product_vote', array(
          'header'    => Mage::helper('productvote')->__('Vote on Page'),
          'align'     =>'left',
          'index'     => 'product_vote',
      ));
	    $this->addColumn('product_xahoi', array(
          'header'    => Mage::helper('productvote')->__('Vote on Social Networking'),
          'align'     =>'left',
          'index'     => 'product_xahoi',
      ));
         $this->addColumn('status', array(
          'header'    => Mage::helper('productvote')->__('Status'),
          'align'     =>'left',
          'index'     => 'status',
      ));

	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('productvote');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('productvote')->__('Delete'),
             'url'      => $this->getUrl('*/*/delete1'),
             'confirm'  => Mage::helper('productvote')->__('Are you sure?')
        ));        
        return $this;       
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}