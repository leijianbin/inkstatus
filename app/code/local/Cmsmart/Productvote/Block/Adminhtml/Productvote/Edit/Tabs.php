<?php

class Cmsmart_Productvote_Block_Adminhtml_Productvote_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('productvote_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('productvote')->__('Edit Status Productvote '));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('productvote')->__('Edit'),
          'title'     => Mage::helper('productvote')->__('Edit'),
          'content'   => $this->getLayout()->createBlock('productvote/adminhtml_productvote_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}