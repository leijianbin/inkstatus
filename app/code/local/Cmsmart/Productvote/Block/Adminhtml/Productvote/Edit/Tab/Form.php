<?php

class Cmsmart_Productvote_Block_Adminhtml_Productvote_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('productvote_form', array('legend'=>Mage::helper('productvote')->__('ProductVote Edit')));
     
      $fieldset->addField('status', 'select', array(
            'label'     => Mage::helper('productvote')->__('Status'),
            'name'      => 'status',
            'value'  => '1',
            'values' => array('Enable' => 'Enable','Disable' => 'Disable')
        ));
      if ( Mage::getSingleton('adminhtml/session')->getDeliverydateData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getDeliverydateData());
          Mage::getSingleton('adminhtml/session')->setDeliverydateData(null);
      } elseif ( Mage::registry('productvote_data') ) {
          $form->setValues(Mage::registry('productvote_data')->getData());
      }
      return parent::_prepareForm();
  }
}