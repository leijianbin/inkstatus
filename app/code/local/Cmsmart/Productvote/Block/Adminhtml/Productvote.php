<?php
class Cmsmart_Productvote_Block_Adminhtml_Productvote extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_productvote';
    $this->_blockGroup = 'productvote';
    $this->_headerText = Mage::helper('productvote')->__('Productvote Manager');  
    parent::__construct();
    $this->_removeButton('add');
  }
}