<?php

class Cmsmart_Productvote_Block_Provoteblock extends Mage_Catalog_Block_Product_View {

    public function numberlikeblock() {
        $_product = $this->getProduct();
        $idproduct = $_product->getId();      
        $collection = Mage::getModel('productvote/productvote');
        $a = $collection->getCollection()->addFieldToFilter('product_id', $idproduct);
        if($a->getSize()>0){           
            $model = $collection->load($a->getFirstItem()->getId());
            $productvote = $model->getProductVote();
        }
        return $productvote;
    }
   

}

?>