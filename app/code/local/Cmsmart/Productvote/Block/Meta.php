<?php

class Cmsmart_Productvote_Block_Meta extends Mage_Core_Block_Template
{

    public function getProduct()
    {
        return Mage::registry('current_product');
    }

    public function isActive()
    {
        return true;
    }

    public function getProductName()
    {
        return $this->escapeHtml($this->getProduct()->getName());
    }

    public function getProductImage()
    {
        return $this->helper('catalog/image')->init($this->getProduct(), 'thumbnail');
    }

    public function getProductUrl()
    {
        return $this->getProduct()->getProductUrl();
    }

}