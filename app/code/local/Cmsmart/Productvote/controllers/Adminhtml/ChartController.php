<?php

class Cmsmart_Productvote_Adminhtml_ChartController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->loadLayout();
        $block = $this->getLayout()->createBlock(
                'Mage_Core_Block_Template', 'productvote/adminhtml_chartvoteblock', array('template' => 'cmsmart/productvote/chartvot.phtml')
        );

        $this->getLayout()->getBlock('content')->append($block);
        $this->renderLayout();
    }

}

?>