<?php

class Cmsmart_Productvote_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }
	public function deletekoAction(){
		$collection = Mage::getModel('productvote/productvote');
		$loginroi = $collection->getCollection()
							   ->addFieldToFilter('product_vote', 0)
							   ->addFieldToFilter('product_xahoi', 0);
		$mang = $loginroi->getData();
		$collection->load($mang[0]['id']);
		$collection->delete();
		$collection->save();
	}
    public function loadpageAction(){
         $id = $this->getRequest()->getPost('id');  
                
                $name = $this->getRequest()->getPost('name');  
                $fa = $this->getRequest()->getPost('fa');
                $tw = $this->getRequest()->getPost('twee');
                $g = $this->getRequest()->getPost('gcong');
                if($fa!=0 || $tw!=0 || $g!=0){
                  $collection = Mage::getModel('productvote/productvote');
                  $a = $collection->getCollection()->addFieldToFilter('product_id', $id);
                  if($a->getSize()>0){  
                    $model = $collection->load($a->getFirstItem()->getId());
                    $model->setProductXahoi($fa+$tw+$g);
                    $model->save();
                }
                else{
                    $insertData['product_id'] = $id;
                    $insertData['product_name'] = $name;
                    $insertData['product_xahoi'] = $fa + $tw + $g;
                    $insertData['status'] = 'Enable';            
                    $collection->setData($insertData);
                    $collection->save();
                }
				}
         if (Mage::helper('customer')->isLoggedIn() == true){
             
			  //
              $customerData = Mage::getSingleton('customer/session')->getCustomer();
              $cusid = $customerData->getId();
              $cus = Mage::getModel('productvote/customervote');
              $loginroi = $cus->getCollection()
                            ->addFieldToFilter('customer_id', $cusid)
                            ->addFieldToFilter('product_id', $id);
              if($loginroi->getSize()>0){           
                       $return['data'] ="likeroi";
                       echo json_encode($return);
                }
               else{
                    $return['data'] ="chualike";
                       echo json_encode($return);
               }
         }
		 else
		 {
			$return['data'] ="okkkkkkk";
                       echo json_encode($return);
		 }
         
    }
    public function likeAction() {
       if (Mage::helper('customer')->isLoggedIn() == true){
                $id = $this->getRequest()->getPost('id');   
                $name = $this->getRequest()->getPost('name');  
//sdfsdf
             $customerData = Mage::getSingleton('customer/session')->getCustomer();
              $cusid = $customerData->getId();
              $cus = Mage::getModel('productvote/customervote');
              $loginroi = $cus->getCollection()
                            ->addFieldToFilter('customer_id', $cusid)
                            ->addFieldToFilter('product_id', $id);
              if($loginroi->getSize()>0){           
                    
                }
               else{  
                    $cus->setProductId($id);
                    $cus->setCustomerId($cusid);
                    $cus->save();
               }
//
               $artist_id = Mage::getModel('marketplace/product')->load($id, 'mageproductid')->getUserid();

                $collection = Mage::getModel('productvote/productvote');
                $a = $collection->getCollection()->addFieldToFilter('product_id', $id);
                
                if($a->getSize()>0){  
                    
                    $model = $collection->load($a->getFirstItem()->getId());
                    $productvote = $model->getProductVote();
					          $model->setProductName($name);
                    $model->setProductVote($productvote + 1);
                    if($artist_id) $model->setProductArtist($artist_id);
                    $model->save();
                }
                else{
                    $insertData['product_id'] = $id;
                    $insertData['product_name'] = $name;
                    $insertData['product_vote'] =1;
                    $insertData['status'] = 'Enable';  
                    if($artist_id) $insertData['product_artist'] = $artist_id;           
                    $collection->setData($insertData);
                    $collection->save();
                }
                $return['data'] ='<span class="like">'.($productvote + 1).'</span>';
                echo json_encode($return);
       }
        else
         {
             $return['data'] = 'false';
             echo json_encode($return);
         }
                
     }
       
    public function changedislikeAction() {
        
        if (Mage::helper('customer')->isLoggedIn() == true){
              $id = $this->getRequest()->getPost('id');  
              $customerData = Mage::getSingleton('customer/session')->getCustomer();
              $cusid = $customerData->getId();
              $cus = Mage::getModel('productvote/customervote');
              $loginroi = $cus->getCollection()
                            ->addFieldToFilter('customer_id', $cusid)
                            ->addFieldToFilter('product_id', $id);
                    $mang = $loginroi->getData();
              $cus->load($mang[0]['id']);
              $cus->delete();
              $cus->save();
              
         }
        
        $id = $this->getRequest()->getPost('id');   
        $name = $this->getRequest()->getPost('name'); 
        
        $collection = Mage::getModel('productvote/productvote');
        $a = $collection->getCollection()->addFieldToFilter('product_id', $id);
        if($a->getSize()>0){           
            $model = $collection->load($a->getFirstItem()->getId());
            $productvote = $model->getProductVote();
			$model->setProductName($name);
            $model->setProductVote($productvote - 1);
            $model->save();
        }       
        $return['data'] = '<span class="unlike">'.($productvote-1).'<span>';
	echo json_encode($return); 
    }
    
    
     protected function _getQuicklogin() {
        return Mage::getSingleton('productvote/quicklogin');
    }
    protected function _getSession() {
        return Mage::getSingleton('customer/session');
    }

    public function loginAction() {
		$result = array();
        if (Mage::helper('customer')->isLoggedIn() != true){
            $quicklogin =  $this->_getQuicklogin();
            $quicklogin->login($_POST['emailL'], $_POST['passwordL']);
			if ($quicklogin->login()) {
				$session = $this->_getSession();
				try {
				$session->login($_POST['emailL'], $_POST['passwordL']);
				$customer = $session->getCustomer();
				$session->setCustomerAsLoggedIn($customer);

				$result['cusname'] = $customer->getName();

				$id = $this->getRequest()->getPost('pid');  
				$cusid = $customer->getId();
				$cus = Mage::getModel('productvote/customervote');
				$loginroi = $cus->getCollection()
				->addFieldToFilter('customer_id', $cusid)
				->addFieldToFilter('product_id', $id);
				if($loginroi->getSize()>0){   
					$result['msg'] = 'likeroi';
					}
				   else{
				$result['msg'] = 'chualike';
				   }
				   
				} catch (Exception $e) {
					$result['msg'] = 'false';
				}
			}
			else
			{
				$result['msg'] = 'false';
			}
        }
		
		echo json_encode($result);
    }

   
  
}

?>
