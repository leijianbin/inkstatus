<?php

class Cmsmart_Productvote_Model_Observer extends Varien_Event_Observer {

    public function delete(Varien_Event_Observer $observer) {

		$idproduct = $observer->getProduct()->getId();
		$collection = Mage::getModel('productvote/productvote');
		$loginroi = $collection->getCollection()->addFieldToFilter('product_id', $idproduct);
		$mang = $loginroi->getData();
		$collection->load($mang[0]['id']);
		$collection->delete();
		$collection->save();
    }

}
