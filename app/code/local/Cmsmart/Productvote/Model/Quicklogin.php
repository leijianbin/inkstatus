<?php

class Cmsmart_Productvote_Model_Quicklogin extends Cmsmart_Productvote_Model_Validate
{
	public function _construct() 
    {
        parent::_construct();
    }
     public function forgotpassword($email2 = '')
    {
        $this->validateEmail($email2);
        if ($this->isEmailExist())
            {
                return true;
            }
           return false;  
    }
    public function login($email = '', $password = ''){
        $this->validateEmail($email);
        $this->validatePassword($password);
        if ($this->getMessage() != '') {
            return true;
        }

          return false;
    }
    public function checkRegister(){
        $this->message = '';
        $this->_CustomerId = -1;
            $this->validateEmail($_POST['email']);
            if ($this->isEmailExist())
            {
                $this->message .=  'Email is not existed';
            }
            else
            {
                $this->setPassword($_POST['password'], $_POST['confirmpassword']);
                $this->setName($_POST['firstname'], $_POST['lastname']);
                $this->setNewsletter($_POST['newsletter']);
        
                if ($this->message == '')
                {
                    $this->register();
                    
                    if ($this->_CustomerNewsletter == true)
                    {
                        $this->subscribe();
                    }
            }
        }       
    }
    private function register()
    {
        $customer = Mage::getModel('customer/customer');
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
        $customer->setEmail($this->_CustomerEmail);
        $customer->setPassword($this->_CustomerPassword);
        $customer->setFirstname($this->_CustomerFirstName);
        $customer->setLastname($this->_CustomerLastName);  
        try
        {
            $customer->save();
            $customer->setConfirmation(null);
            $customer->save();   
            $storeId = $customer->getSendemailStoreId();
            $customer->sendNewAccountEmail('registered', '', $storeId);   
            Mage::getSingleton('customer/session')->loginById($customer->getId());
            $this->_CustomerId = $customer->getId();
            $this->message = 'true';
        }
        catch (Exception $e)
        {
            $this->message = $e->getMessage();
        }
    }
    
    private function subscribe()
    {
        $subscriber = Mage::getModel('newsletter/subscriber')->loadByEmail($this->_CustomerEmail);
        if (!$subscriber->getId())
        {
            $subscriber->setStatus(Mage_Newsletter_Model_Subscriber::STATUS_SUBSCRIBED);
            $subscriber->setSubscriberEmail($this->_CustomerEmail);
            $subscriber->setSubscriberConfirmCode($subscriber->RandomSequence());
        }
        $subscriber->setStoreId(Mage::app()->getStore()->getId());
        $subscriber->setCustomerId($this->_CustomerId);
        try
        {
            $subscriber->save();
        }
        catch (Exception $e)
        {
             $this->message = $e->getMessage();
        }
    }
}
