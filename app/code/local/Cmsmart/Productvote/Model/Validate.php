<?php

class Cmsmart_Productvote_Model_Validate extends Varien_Object
{ 

    protected $_CustomerEmail;

    protected $_CustomerPassword;
    
    protected $_CustomerFirstname;

    protected $_CustomerLastname;

    protected $_CustomerNewletter;
    
    protected $_CustomerId;

    protected $message;

	public function _construct() 
    {
        parent::_construct();
    }

    protected function validateEmail($email = '')
    {
    	 if (!Zend_Validate::is($email, 'EmailAddress'))
        {
            $this->message .= ' Invalid email address,';
        }
        else
        {
            $this->_CustomerEmail = $email;
        }
    }
    protected function validatePassword($password){

        $replacePassword = str_replace(array('\'', '%', '\\', '/', ' '), '', $password);
        
        if (strlen($replacePassword) < 6 || $replacePassword != trim($password))
        {
            $this->message .= ' Error password,';
        }
        
        $this->_CustomerPassword = $replacePassword;
    }

     protected function setPassword($password = '', $confirmation = '')
    {        
        $sanitizedPassword = str_replace(array('\'', '%', '\\', '/', ' '), '', $password);
        
        if ($password != $sanitizedPassword)
        {
            $this->message .= ' Password is required,';
            return true;
        }
        
        if (strlen($sanitizedPassword) < 6)
        {
            $this->message .= ' Password, Please enter 6 or more characters,';
            return true;
        }
        
        
        if ($sanitizedPassword != $confirmation)
        {
            $this->message .= 'Please make sure your passwords match,';
            return true;
        }
        
        $this->_CustomerPassword = $sanitizedPassword;
    }
    
    protected function setName($firstname = '', $lastname = '')
    {
        $stop = false;
        if (!Zend_Validate::is( trim($firstname) , 'NotEmpty')) 
        { 
            $this->message .= ' Firstname is required,';
            $stop = true;
        }
        
       if (!Zend_Validate::is( trim($lastname) , 'NotEmpty')) 
        {
            $this->message .= ' Lastname is required,';
            $stop = true;
        }
        
        if ($stop == true)
        {
            return true;
        }
        
        if ($stop != true)
        {
            $this->_CustomerFirstName = $firstname;
            $this->_CustomerLastName = $lastname;
        }
    }
    
    protected function setNewsletter($newsletter = '0')
    {
        if ($newsletter == '1')
        {
            $this->_CustomerNewsletter = true;
        }
        else
        {
            $this->_CustomerNewsletter = false;
        }
    }
    protected function isEmailExist()
    {
        $customer = Mage::getModel('customer/customer');
        
        $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
        $customer->loadByEmail($this->_CustomerEmail);
        
        if($customer->getId())
        {
            return true;
        }

        return false;
    }
    
    public function getMessage()
    {
        return $this->message;
    }

}	