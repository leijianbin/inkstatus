<?php

class Cmsmart_Productvote_Helper_Data extends Mage_Core_Helper_Data {
	
    public function producvotechart() {
        $collection = Mage::getModel('productvote/productvote');
        $a = $collection->getCollection();
		$b = $a->getData();
		$mang=array();
		foreach($b as $bb){
			 $mang[] = array(
                    'id' => $bb['id'],
                    'product_id' =>$bb['product_id'],
					'product_vote' => $bb['product_vote'],
					'product_xahoi' => $bb['product_xahoi'],
					'product_name' =>$bb['product_name'],
					'nuvote' => $bb['product_vote']+ $bb['product_xahoi'],
					'status'=>$bb['status']
                );
		}
		
		$arr2 = $this->array_msort($mang, array('nuvote'=>SORT_DESC));
		return $arr2;
    }
	function array_msort($array, $cols)
	{
		$colarr = array();
		foreach ($cols as $col => $order) {
			$colarr[$col] = array();
			foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
		}
		$eval = 'array_multisort(';
		foreach ($cols as $col => $order) {
			$eval .= '$colarr[\''.$col.'\'],'.$order.',';
		}
		$eval = substr($eval,0,-1).');';
		eval($eval);
		$ret = array();
		foreach ($colarr as $col => $arr) {
			foreach ($arr as $k => $v) {
				$k = substr($k,1);
				if (!isset($ret[$k])) $ret[$k] = $array[$k];
				$ret[$k][$col] = $array[$k][$col];
			}
		}
		return $ret;

	}
    public function percentvote($pro) {
        $vote = array();
        $proname = array();
        foreach ($pro as $v) {
            $vote[] = $v['product_vote']+$v['product_xahoi'];
            $proname[] = $v['product_name'];
        }
        return array_sum($vote);
    }

    public function enablevote($idprovote) {
        $collection = Mage::getModel('productvote/productvote');
        $a = $collection->getCollection()->addFieldToFilter('product_id', $idprovote);
        if ($a->getSize() > 0) {
            $model = $collection->load($a->getFirstItem()->getId());
            $status = $model->getStatus();
            return $status;
        }
        else
            return "Enable";
    }
    public function getlike($url){
        $url = $url;
        $json_string = file_get_contents('http://graph.facebook.com/?ids=' . $url);

        $json = json_decode($json_string, true);

        $face = ($json[$url]['shares']);
        $json_string = file_get_contents('http://urls.api.twitter.com/1/urls/count.json?url=' . $url);

        $json = json_decode($json_string, true);

        $tweet = ($json['count']);
        $ch = curl_init();
        curl_setopt_array($ch, array(
        CURLOPT_HTTPHEADER      => array('Content-type: application/json'),
        CURLOPT_POST            => true,
        CURLOPT_POSTFIELDS      => '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"'.$url.'","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]',
        CURLOPT_RETURNTRANSFER  => true,
        CURLOPT_SSL_VERIFYPEER  => false,
        CURLOPT_URL             => 'https://clients6.google.com/rpc?key=AIzaSyCKSbrvQasunBoV16zDH9R33D88CeLr9gQ'
        ));
        $res = curl_exec($ch);
        curl_close($ch);
        $json = json_decode($res,true);
        $gcong = $json[0]['result']['metadata']['globalCounts']['count'];
        
        $manglike = array(
                        "face" => $face,
                        "tweet" => $tweet,
                        "gcong" => $gcong
                    );
        return $manglike;
    }

}