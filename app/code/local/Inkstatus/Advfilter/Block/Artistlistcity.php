<?php

class Inkstatus_Advfilter_Block_Artistlistcity extends Mage_Core_Block_Template {

	public function _prepareLayout() {
		return parent::_prepareLayout();
    }

    public function getArtists() {
    	$city_code = $this->getCityCode();
    	$page_size = $this->getPerPage();
    	$current_page = $this->getCurrentPage();
    	$sortby = $this->getSortBy();

		$collectionfetch = Mage::getModel('marketplace/userprofile')->getCollection()
				->addFieldToFilter('partnerstatus',array('eq'=>'Seller'))
				->addFieldToFilter('wantpartner',array('eq'=>1));

		if($city_code) $collectionfetch->addFieldToFilter('complocality',array('eq'=>$city_code));	
		// $collection->setOrder('autoid','DESC');
		// $collection->setPageSize($page_size);	
		// $collection->setCurPage($current_page);			 


		$record = array();
		foreach($collectionfetch as $id){
			$record[] = $id->getmageuserid();
		}
		if(count($record)!=0){
			$collection = Mage::getModel('customer/customer')->getCollection();
			// 	->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
			// 	->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left')
			$collection->addNameToSelect();
			$collection->addAttributeToFilter('entity_id', array('in' => $record));
			// if($country_code) {
			// 	$collection->addAttributeToFilter('billing_country_id', array('eq' => $country_code));
			// }
			$collection->joinTable('marketplace/userprofile', 'mageuserid=entity_id', array('*'), null, 'left');
			
			if($sortby) {
		    	if($sortby == 'nameup') {
		    		$collection->addAttributeToSort('firstname', 'ASC');
		    	} elseif($sortby == 'namedown') {
		    		$collection->addAttributeToSort('firstname', 'DESC');
		    	}
		    }

			$collection->setPageSize($page_size);
	    	$collection->setCurPage($current_page);
		} else {
			$collection = $collectionfetch;
			$collection->addFieldToFilter('mageuserid', array('eq' => -1));
		}

		return $collection;
    }


}