<?php

class Inkstatus_Advfilter_Block_Productlist extends Mage_Core_Block_Template {

	public function _prepareLayout() {
		return parent::_prepareLayout();
    }

    public function getProducts() {
    	$category_id = $this->getCategoryId();
    	$category = Mage::getModel('catalog/category')->load($category_id);
    	$page_size = $this->getPerPage();
    	$current_page = $this->getCurrentPage();
    	$sortby = $this->getSortBy();

    	$products = Mage::getModel('catalog/product')
	    ->getCollection()
		->addAttributeToSelect('*')
	    ->addCategoryFilter($category)
	    ->setPageSize($page_size)
	    ->setCurPage($current_page);

	    if($sortby) {
	    	if($sortby == 'nameup') {
	    		$products->addAttributeToSort('name', 'ASC');
	    	} elseif($sortby == 'namedown') {
	    		$products->addAttributeToSort('name', 'DESC');
	    	} elseif($sortby == 'priceup') {
	    		$products->addAttributeToSort('price', 'ASC');
	    	} elseif($sortby == 'pricedown') {
	    		$products->addAttributeToSort('price', 'DESC');
	    	}
	    }
	    $products->load();

	    return $products;
    }


}