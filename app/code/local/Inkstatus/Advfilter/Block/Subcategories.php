<?php

class Inkstatus_Advfilter_Block_Subcategories extends Mage_Core_Block_Template {

	public function _prepareLayout() {
		return parent::_prepareLayout();
    }

    public function getCategories() {
    	$category_id = $this->getCategoryId();
    	return Mage::getModel('catalog/category')->getCategories($category_id);
    }


}