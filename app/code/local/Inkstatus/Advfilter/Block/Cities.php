<?php

class Inkstatus_Advfilter_Block_Cities extends Mage_Core_Block_Template {

	public function _prepareLayout() {
		return parent::_prepareLayout();
    }

    public function getCities() {
    	// $collectionfetch = Mage::getModel('marketplace/userprofile')->getCollection();
		// $record = array();
		// foreach($collectionfetch as $id)
		// {
		// 	$record[] = $id->getmageuserid();
		// }
		// if(count($record)!=0){	
		// 	$collection = Mage::getModel('customer/customer')->getCollection()
		// 		->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
		// 		->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left');
		// 	$collection->addAttributeToFilter('entity_id', array('in' => $record));
		// }

    	$country = $this->getCountryCode();
		$collection = Mage::getModel('marketplace/userprofile')->getCollection()->addFieldToFilter('countrypic', array('eq' => $country));
		$cities = array();
    	foreach($collection as $id) {
    		if(!in_array($id->getcomplocality(), $cities)) {
    			$cities[] = $id->getcomplocality();
    		}
    	}

    	return $cities;
    }


}