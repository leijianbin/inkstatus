<?php

class Inkstatus_Advfilter_Block_Countries extends Mage_Core_Block_Template {

	public function _prepareLayout() {
		return parent::_prepareLayout();
    }

    public function getCountries() {
    	// $collectionfetch = Mage::getModel('marketplace/userprofile')->getCollection();
		// $record = array();
		// foreach($collectionfetch as $id)
		// {
		// 	$record[] = $id->getmageuserid();
		// }
		// if(count($record)!=0){	
		// 	$collection = Mage::getModel('customer/customer')->getCollection()
		// 		->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
		// 		->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left');
		// 	$collection->addAttributeToFilter('entity_id', array('in' => $record));
		// }

		$collection = Mage::getModel('marketplace/userprofile')->getCollection();
		$countries = array();
    	foreach($collection as $id) {
    		if(!in_array($id->getcountrypic(), $countries)) {
    			$countries[] = $id->getcountrypic();
    		}
    	}

    	return $countries;
    }


}