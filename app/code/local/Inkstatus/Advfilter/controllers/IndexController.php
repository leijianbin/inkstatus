<?php

class Inkstatus_Advfilter_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction(){
		$this->loadLayout(); 
		$this->renderLayout();
    }

    // filter by category, return json format product list html and subcategories
    public function subcategoryAction() {
    	$id = $this->getRequest()->getParam('id');

    	if($id) {

			$category = Mage::getModel('catalog/category')->load($id);
			if($category->getChildren()) {
	    		$subcategories = $this->getLayout()->createBlock('advfilter/subcategories')
				    ->setCategoryId($id)
				    ->setUseAjax(true)
				    ->setTemplate('advfilter/subcategories.phtml')
				    ->toHtml();				
				$result['subcategories'] = $subcategories;
			} else {
				$result['subcategories'] = "";
			}

    	}

    	if($result) {
    		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    	}

    }

	public function categoryproductsAction() {
		$id = $this->getRequest()->getParam('id');
		$page = $this->getRequest()->getParam('page');
		$perpage = $this->getRequest()->getParam('perpage');
		$sortby = $this->getRequest()->getParam('sortby');

		$productlist = $this->getLayout()->createBlock('advfilter/productlist')
			    ->setCategoryId($id)
			    ->setPerPage($perpage)
			    ->setCurrentPage($page)
			    ->setSortBy($sortby)
			    ->setUseAjax(true)
			    ->setTemplate('advfilter/productlist.phtml')
			    ->toHtml();
		$result['productlist'] = $productlist;

    	if($result) {
    		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    	}
	}

    public function artistcountriesAction() {
    	$countries = $this->getLayout()->createBlock('advfilter/countries')
		    ->setUseAjax(true)
		    ->setTemplate('advfilter/countries.phtml')
		    ->toHtml();

		$result['countries'] = $countries;

    	if($result) {
    		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    	}    	

    }

    public function artistcitiesAction() {
    	$code = $this->getRequest()->getParam('code');

    	$cities = $this->getLayout()->createBlock('advfilter/cities')
    		->setCountryCode($code)
		    ->setUseAjax(true)
		    ->setTemplate('advfilter/cities.phtml')
		    ->toHtml();

		$result['cities'] = $cities;

    	if($result) {
    		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    	}    	

    }

    public function artistlistAction() {
		$code = $this->getRequest()->getParam('code');
		$page = $this->getRequest()->getParam('page');
		$perpage = $this->getRequest()->getParam('perpage');
		$sortby = $this->getRequest()->getParam('sortby');

		$artistlist = $this->getLayout()->createBlock('advfilter/artistlist')
			    ->setCountryCode($code)
			    ->setPerPage($perpage)
			    ->setCurrentPage($page)
			    ->setSortBy($sortby)
			    ->setUseAjax(true)
			    ->setTemplate('advfilter/artistlist.phtml')
			    ->toHtml();
		$result['artistlist'] = $artistlist;

		// $collectionfetch = Mage::getModel('marketplace/userprofile')->getCollection()
		// 		->addFieldToFilter('partnerstatus',array('eq'=>'Seller'))
		// 		->addFieldToFilter('wantpartner',array('eq'=>1));								 
		// $record = array();
		// foreach($collectionfetch as $id){
		// 	$record[] = $id->getmageuserid();
		// }

		// $collection = Mage::getModel('customer/customer')->getCollection()
		// 	->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
		// 	->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left')
		// 	->addNameToSelect();
		// $collection->addAttributeToFilter('entity_id', array('in' => $record));
		// if($code) {
		// 	$collection->addAttributeToFilter('billing_country_id', array('eq' => $code));
		// }		
		// $collection->joinTable('marketplace/userprofile', 'mageuserid=entity_id', array('*'), null, 'left');


    	if($result) {
    		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    	}  
    }

    public function artistlistcityAction() {
		$code = $this->getRequest()->getParam('code');
		$page = $this->getRequest()->getParam('page');
		$perpage = $this->getRequest()->getParam('perpage');
		$sortby = $this->getRequest()->getParam('sortby');

		$artistlist = $this->getLayout()->createBlock('advfilter/artistlistcity')
			    ->setCityCode($code)
			    ->setPerPage($perpage)
			    ->setCurrentPage($page)
			    ->setSortBy($sortby)
			    ->setUseAjax(true)
			    ->setTemplate('advfilter/artistlist.phtml')
			    ->toHtml();
		$result['artistlist'] = $artistlist;

    	if($result) {
    		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    	}  
    }

    public function hitinfoAction() {
    	$productid = $this->getRequest()->getParam('id');
    	$result['hitip'] = $_SERVER['REMOTE_ADDR'];
    	$model = Mage::getModel('marketplace/product')->load($productid, 'mageproductid');
    	$result['lasthitip'] = $model->getLasthitip();
    	$result['lasthittime'] = $model->getLasthittime();

    	if($result) {
    		$this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    	}
    }

    public function updateinfoAction() {
    	$productid = $this->getRequest()->getParam('id');
    	$hitip = $_SERVER['REMOTE_ADDR'];
    	$timestamp = date('Y-m-d G:i:s');

    	$model = Mage::getModel('marketplace/product')->load($productid, 'mageproductid');
    	$id = $model->getIndexId();
    	$hitcount = $model->getHitcount() + 1;
    	$data = array('hitcount'=>$hitcount, 'lasthitip'=>$hitip, 'lasthittime'=>$timestamp);
    	$model->addData($data);
		try {
		    $model->setId($id)->save();
		    echo "Data updated successfully.";
		} catch (Exception $e){
		    echo $e->getMessage(); 
		}

    }

}