<?php
	class Webkul_Mpsellerbuyercomm_Block_Sellerbuyercomm extends Mage_Customer_Block_Account	{
		
		public function __construct()    {
            parent::__construct();
            
    		$customerid = Mage::getSingleton("customer/session")->getCustomerId();
    		$collection = Mage::getModel("mpsellerbuyercomm/sellerbuyercomm")->getCollection()
                            ->addFieldToFilter("customerid",$customerid);
            $this->setCollection($collection);
        }

        protected function _prepareLayout()    {
            parent::_prepareLayout(); 
            $pager = $this->getLayout()->createBlock("page/html_pager","custom.pager");
            $pager->setAvailableLimit(array(9=>9,15=>15,30=>30,"all"=>"all"));
            $pager->setCollection($this->getCollection());
            $this->setChild("pager",$pager);
            $this->getCollection()->load();
            return $this;
        }

        public function getPagerHtml()   {
            return $this->getChildHtml("pager");
        }

	}