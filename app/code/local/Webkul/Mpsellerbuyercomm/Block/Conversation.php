<?php
	class Webkul_Mpsellerbuyercomm_Block_Conversation extends Mage_Customer_Block_Account	{
		
		public function __construct()    {
            
            parent::__construct();
    		$id = $this->getRequest()->getParam("id");
    		$collection = Mage::getModel("mpsellerbuyercomm/conversation")->getCollection()
                            ->addFieldToFilter("commid",$id);
            $this->setCollection($collection);
        }

        protected function _prepareLayout()    {
            parent::_prepareLayout(); 
            $pager = $this->getLayout()->createBlock("page/html_pager","custom.pager");
            $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,"all"=>"all"));
            $pager->setCollection($this->getCollection());
            $this->setChild("pager",$pager);
            $this->getCollection()->load();
            return $this;
        }

        public function getPagerHtml()   {
            return $this->getChildHtml("pager");
        }

	}