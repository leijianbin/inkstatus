<?php
	class Webkul_Mpsellerbuyercomm_Block_Adminhtml_Selleradminqueryview extends Mage_Adminhtml_Block_Widget_Grid_Container {

	    public function __construct() {
	        $this->_controller = "adminhtml_selleradminqueryview";
	        $this->_blockGroup = "mpsellerbuyercomm";
	        $this->_headerText = Mage::helper("mpsellerbuyercomm")->__("Conversation History");
	        $data = array(
                'label' =>  'Reply to seller',
                'onclick'   => "setLocation('".$this->getUrl('*/*/new/',array('id'=>$this->getRequest()->getParam('id')
                	))."')"
                );
	        $this->addButton('Reply to seller', $data, 0, 100,  'header', 'header');
	        parent::__construct();
	        $this->_removeButton("add");
	    }
	}