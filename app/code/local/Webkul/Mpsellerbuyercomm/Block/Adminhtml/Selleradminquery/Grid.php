<?php 
class Webkul_Mpsellerbuyercomm_Block_Adminhtml_Selleradminquery_Grid extends Mage_Adminhtml_Block_Widget_Grid {
    public function __construct() {
        parent::__construct();
        $this->setId("id");
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel("mpsellerbuyercomm/sellerbuyercomm")->getCollection()
                        ->addFieldToFilter('admintype',array('eq'=>'1'));
        $prefix = Mage::getConfig()->getTablePrefix();

        $fnameid = Mage::getModel("eav/entity_attribute")->loadByCode("1", "firstname")->getAttributeId();
        $lnameid = Mage::getModel("eav/entity_attribute")->loadByCode("1", "lastname")->getAttributeId();

        $collection->getSelect()
        ->join(array("ce3" => "customer_entity_varchar"),"ce3.entity_id = main_table.sellerid",array("fname" => "value"))->where("ce3.attribute_id = ".$fnameid)
        ->join(array("ce4" => "customer_entity_varchar"),"ce4.entity_id = main_table.sellerid",array("lname" => "value"))->where("ce4.attribute_id = ".$lnameid)
        ->columns(new Zend_Db_Expr("CONCAT(`ce3`.`value`, ' ',`ce4`.`value`) AS sellername"));
        $collection->addFilterToMap("sellername","CONCAT(`ce3`.`value`, ' ',`ce4`.`value`)");

        $this->setCollection($collection);
        parent::_prepareCollection();

        foreach ($this->getCollection() as $item) {
            $item->sellername=sprintf('<a href="%s" title="View Customer">%s</a>',
                                                 $this->getUrl("adminhtml/customer/edit",array("id"=>$item->getSellerid())),$item->getSellername());

            if($item->getProductid()){
                $proname = Mage::getModel('catalog/product')->load($item->getProductid())->getName();
                $item->proname = sprintf('<a href="%s" title="View product">%s</a>',
                                                 $this->getUrl('adminhtml/catalog_product/edit/id/' . $item->getProductid()),$proname
                                                );
            }else{
                $item->proname = sprintf('<a href="%s" title="View product">%s</a>',
                                                 '#','none'
                                                );
            }

            $item->view = sprintf('<a href="%s" title="View">View</a>',$this->getUrl("*/adminhtml_selleradminquery/index", array("id" => $item->getId())));
        }

    }

    protected function _prepareColumns() {

        $this->addColumn("id", array(
            "header"    =>  Mage::helper("mpsellerbuyercomm")->__("Id"),
            "align"     =>  "center",
            "width"     =>  "50px",
            "index"     =>  "id",
            "type"      =>  "range"
        ));

        $this->addColumn('proname', array(
            'header'        => Mage::helper('mpsellerbuyercomm')->__('Product Name'),
            'index'         => 'proname',
            'type'          => 'text',
            'filter'    => false,
            'sortable'  => false,  
        ));

        $this->addColumn("sellername", array(
            "header"    =>  Mage::helper("mpsellerbuyercomm")->__("Seller Name"),
            "align"     =>  "center",
            "width"     =>  "100px",
            'type'          => 'text',
            "index"     =>  "sellername",
            "filter_index"=> "sellername"
        ));

        $this->addColumn("subject", array(
            "header"    =>  Mage::helper("mpsellerbuyercomm")->__("Subject"),
            "align"     =>  "left",
            "index"     =>  "subject"
        ));

        $this->addColumn("created_at", array(
            "header"    =>  Mage::helper("mpsellerbuyercomm")->__("Date"),
            "align"     =>  "left",
            "width"     =>  "150px",
            "index"     =>  "created_at",
            "type"      =>  "datetime",
            "filter_index"=> "main_table.created_at"
        ));

        $this->addColumn('view', array(
            'header'        => Mage::helper('mpsellerbuyercomm')->__('View'),
            'index'         => 'view',
            'type'          => 'text',
            'filter'    => false,
            'sortable'  => false,  
        ));

        $this->addExportType("*/*/exportCsv", Mage::helper("mpsellerbuyercomm")->__("CSV"));
        $this->addExportType("*/*/exportXml", Mage::helper("mpsellerbuyercomm")->__("XML"));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField("id");
        $this->getMassactionBlock()->setFormFieldName("ids");
        $this->getMassactionBlock()->addItem("delete", array(
            "label"      => Mage::helper("mpsellerbuyercomm")->__("Delete"),
            "url"        => $this->getUrl("*/*/massDeletesbcomm"),
            "confirm"    => Mage::helper("mpsellerbuyercomm")->__("Are you sure")."?"
        ));
        return $this;
    }

    public function getRowUrl($row) {
        return $this->getUrl("*/adminhtml_selleradminquery/index", array("id" => $row->getId()));
    }

    public function getGridUrl()    {
        return $this->getUrl("*/*/grid", array("_current" => true));
    }
}