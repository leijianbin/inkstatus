<?php
class Webkul_Mpsellerbuyercomm_Block_Adminhtml_Selleradminqueryview_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs{

  public function __construct()
  {
      parent::__construct();
      $this->setId('selleradmin_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('mpsellerbuyercomm')->__('Seller Admin Communication'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('mpsellerbuyercomm')->__('Send Reply to Seller'),
          'title'     => Mage::helper('mpsellerbuyercomm')->__('Send Reply to Seller'),
          'content'   => $this->getLayout()->createBlock('mpsellerbuyercomm/adminhtml_selleradminqueryview_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}