<?php
class Webkul_Mpsellerbuyercomm_Block_Adminhtml_Selleradminqueryview_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {
    public function __construct() {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'mpsellerbuyercomm';
        $this->_controller = 'adminhtml_selleradminqueryview';        
        $this->_updateButton('save', 'label', Mage::helper('mpsellerbuyercomm')->__('Send'));
        $this->_removeButton('delete');
        $this->_removeButton('back');
        $this->_removeButton('reset');

        $data = array(
            'label' =>  'Back',
            'class' =>  'back',
            'onclick'   => "setLocation('".$this->getUrl('*/*/',array('id'=>$this->getRequest()->getParam('id')
                ))."')"
        );

        $this->addButton('Back', $data,0);

        $data = array(
            'label' =>  'Reset',
            'onclick'   => "setLocation(window.location.href)"
        );

        $this->addButton('Reset', $data);
    }

    public function getHeaderText()
    {        
        return Mage::helper('mpsellerbuyercomm')->__('Reply to Seller');
    }
}