<?php
class Webkul_Mpsellerbuyercomm_Block_Adminhtml_Selleradminqueryview_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {
  protected function _prepareForm()  {
    $form = new Varien_Data_Form();
    $this->setForm($form);
    $fieldset = $form->addFieldset('selleradmin_form', array('legend'=>Mage::helper('mpsellerbuyercomm')->__('Add a Message')));
       
    $fieldset->addField('message', 'textarea', array(
        'label'     => Mage::helper('mpsellerbuyercomm')->__('Enter Message'),
        'class'     => 'required-entry',
        'required'  => true,
        'name'      => 'message',
    ));    
    return parent::_prepareForm();
  }
}