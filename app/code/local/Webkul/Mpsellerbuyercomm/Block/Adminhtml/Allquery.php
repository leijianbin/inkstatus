<?php
	class Webkul_Mpsellerbuyercomm_Block_Adminhtml_Allquery extends Mage_Adminhtml_Block_Widget_Grid_Container {

	    public function __construct() {
	        $this->_controller = "adminhtml_allquery";
	        $this->_blockGroup = "mpsellerbuyercomm";
	        $this->_headerText = Mage::helper("mpsellerbuyercomm")->__("All Query List");
	        parent::__construct();
	        $this->_removeButton("add");
	    }

	}