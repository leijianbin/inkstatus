<?php 
class Webkul_Mpsellerbuyercomm_Block_Adminhtml_Allquery_View extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId("commid");
        $this->setDefaultSort('id');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $commid = $this->getRequest()->getParam('id');
        $collection = Mage::getModel("mpsellerbuyercomm/conversation")->getCollection()
                        ->addFieldToFilter('commid',array('eq'=>$commid));
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {

        $this->addColumn("id", array(
            "header"    =>  Mage::helper("mpsellerbuyercomm")->__("Id"),
            "align"     =>  "center",
            "width"     =>  "200px",
            "index"     =>  "id",
            "type"      =>  "range"
        ));

        $this->addColumn("message", array(
            "header"    =>  Mage::helper("mpsellerbuyercomm")->__("Message"),
            "align"     =>  "left",
            "index"     =>  "message"
        ));

        $this->addColumn("sender", array(
            "header"    =>  Mage::helper("mpsellerbuyercomm")->__("Send By"),
            "align"     =>  "left",
            "index"     =>  "sender"
        ));

        $this->addColumn("created_at", array(
            "header"    =>  Mage::helper("mpsellerbuyercomm")->__("Send At"),
            "align"     =>  "center",
            "type"      =>  "datetime",
            "align"     =>  "center",
            "width"     =>  "200px",
            "index"     =>  "created_at",                
        ));

        $this->addExportType("*/*/exportconversationCsv", Mage::helper("mpsellerbuyercomm")->__("CSV"));
        $this->addExportType("*/*/exportconversationXml", Mage::helper("mpsellerbuyercomm")->__("XML"));
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField("commid");
        $this->getMassactionBlock()->setFormFieldName("commids");
        $this->getMassactionBlock()->addItem("delete", array(
            "label"      => Mage::helper("mpsellerbuyercomm")->__("Delete"),
            "url"        => $this->getUrl("*/*/massDeleteconversation",array('id' => $this->getRequest()->getParam('id'))),
            "confirm"    => Mage::helper("mpsellerbuyercomm")->__("Are you sure")."?"
        ));
        return $this;
    }

    public function getGridUrl()    {
        return $this->getUrl("*/*/gridview", array("_current" => true));
    }

}