<?php
	class Webkul_Mpsellerbuyercomm_Block_Selleradmincomm extends Mage_Customer_Block_Account	{
		
		public function __construct()    {
            parent::__construct();
            
    		$customerid = Mage::getSingleton("customer/session")->getCustomerId();
            
            $collection = Mage::getModel("mpsellerbuyercomm/sellerbuyercomm")->getCollection()
                        ->addFieldToFilter('admintype',array('eq'=>'1'))
                        ->addFieldToFilter('sellerid',array('eq'=>$customerid));
            $prefix = Mage::getConfig()->getTablePrefix();
            // $collection->getSelect()
            //     ->join(array("ccp" => $prefix."wk_mpsellerbuyercomm_conversation"),"ccp.commid = main_table.id");

            $fnameid = Mage::getModel("eav/entity_attribute")->loadByCode("1", "firstname")->getAttributeId();
            $lnameid = Mage::getModel("eav/entity_attribute")->loadByCode("1", "lastname")->getAttributeId();

            $collection->getSelect()
            ->join(array("ce3" => "customer_entity_varchar"),"ce3.entity_id = main_table.sellerid",array("fname" => "value"))->where("ce3.attribute_id = ".$fnameid)
            ->join(array("ce4" => "customer_entity_varchar"),"ce4.entity_id = main_table.sellerid",array("lname" => "value"))->where("ce4.attribute_id = ".$lnameid)
            ->columns(new Zend_Db_Expr("CONCAT(`ce3`.`value`, ' ',`ce4`.`value`) AS sellername"));
            $collection->addFilterToMap("sellername","CONCAT(`ce3`.`value`, ' ',`ce4`.`value`)");
            $this->setCollection($collection);
        }

        protected function _prepareLayout()    {
            parent::_prepareLayout(); 
            $pager = $this->getLayout()->createBlock("page/html_pager","custom.pager");
            $pager->setAvailableLimit(array(9=>9,15=>15,30=>30,"all"=>"all"));
            $pager->setCollection($this->getCollection());
            $this->setChild("pager",$pager);
            $this->getCollection()->load();
            return $this;
        }

        public function getPagerHtml()   {
            return $this->getChildHtml("pager");
        }

	}