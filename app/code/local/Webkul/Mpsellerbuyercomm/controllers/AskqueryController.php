<?php
class Webkul_Mpsellerbuyercomm_AskqueryController extends Mage_Core_Controller_Front_Action
{
    public function savenewmsgAction(){ 
    	if($_POST['product-id'])       	
			$productid 	= 	$_POST['product-id'];
		else
			$productid 	= 	'';
			$sellerid 	= 	$_POST['seller-id'];
			$ask 		= 	$_POST['ask'];
			$subject 	= 	$_POST['subject'];        		
			$email 		= 	$_POST['email'];
			$buyer 		= 	Mage::getModel('customer/customer')->getCollection()->addFieldToFilter('email',array('eq'=>$email));
		foreach ($buyer as $value) {
			$buyerid = $value->getId();
			$buyername = Mage::getModel('customer/customer')->load($buyerid)->getName();
		}
		if($buyerid){
        	$model = Mage::getModel("mpsellerbuyercomm/sellerbuyercomm");
            $model->setProductid($productid);
            $model->setSellerid($sellerid);
            $model->setCreatedAt(time());
            $model->setCustomerid($buyerid);
            $model->setSubject($subject);
            $lastid = $model->save()->getId();

            $model = Mage::getModel("mpsellerbuyercomm/conversation");
            $model->setCommid($lastid);
            $model->setMessage($ask);
            $model->setSender($buyername);
            $model->setSendertype(0);
            $model->setCreatedAt(time());
            $model->save();
        }

        $emailTemplate 			= 	Mage::getModel('core/email_template')->loadDefault('mpsellerbuyercomm_email');
		$emailTemplateVariables = 	array();
		$mail 					=	Mage::getModel('customer/customer')->load($_POST['seller-id']);
		$sellerEmail 			= 	$mail->getEmail();

		$emailTemplateVariables['myvar1'] 	=	$mail->getName();		
		$emailTemplateVariables['myvar3'] 	=	Mage::getModel('catalog/product')->load($_POST['product-id'])->getName();
		$emailTemplateVariables['myvar4'] 	=	$_POST['ask'];
		$emailTemplateVariables['productid']=	$productid;
		$emailTemplateVariables['subject'] 	=	$_POST['subject'];
		$emailTemplateVariables['url'] 		=	Mage::getUrl('mpsellerbuyercomm/seller/view/',array('id'=>$lastid));
		$emailTemplateVariables['myvar5'] 	=	$_POST['email'];
		
		if(!$buyerid){
			$buyername	=	Mage::getSingleton('customer/session')->getCustomer()->getName() ;
			if(strlen($buyername)<2){$buyername="Guest";}
		}		
		$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
		$emailTemplate->setSenderName($buyername);
		$emailTemplate->setSenderEmail($_POST['email']);
		$emailTemplate->send($sellerEmail,$buyername,$emailTemplateVariables);

		if(Mage::getStoreConfig('mpsellerbuyercomm/admin/notification')==1){
			$admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
			$adminemail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
			$adminname = 'Admin';
			// $adminemail = 	Mage::getStoreConfig("trans_email/ident_general/email");
   //          $adminname 	= 	Mage::getStoreConfig("trans_email/ident_general/name");
            $mail 		=	Mage::getModel('customer/customer')->load($_POST['seller-id']);

	        $emailTemplate 			= 	Mage::getModel('core/email_template')->loadDefault('adminmpsellerbuyercomm_email');
			$emailTemplateVariables = 	array();			
			$emailTemplateVariables['myvar3'] 	=	Mage::getModel('catalog/product')->load($_POST['product-id'])->getName();
			$emailTemplateVariables['myvar4'] 	=	$_POST['ask'];
			$emailTemplateVariables['productid']=	$productid;
			$emailTemplateVariables['subject'] 	=	$_POST['subject'];
			$emailTemplateVariables['url'] 		=	Mage::getUrl('mpsellerbuyercomm/adminhtml_index/view',array('id'=>$lastid));
			$emailTemplateVariables['myvar5'] 	=	$_POST['email'];			
			if(!$buyerid){
				$buyername =Mage::getSingleton('customer/session')->getCustomer()->getName() ;
				if(strlen($buyername)<2){$buyername="Guest";}
			}		
			$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
			$emailTemplate->setSenderName($buyername);
			$emailTemplate->setSenderEmail($_POST['email']);
			$emailTemplate->send($adminemail,$adminname,$emailTemplateVariables);
		}
		echo json_encode("true");
    }
}