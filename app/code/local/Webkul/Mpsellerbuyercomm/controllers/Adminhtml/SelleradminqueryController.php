<?php
    class Webkul_Mpsellerbuyercomm_Adminhtml_SelleradminqueryController extends Mage_Adminhtml_Controller_Action {

        protected function _initAction() {
            $this->loadLayout()->_setActiveMenu("marketplace");
            $this->getLayout()->getBlock("head")->setTitle(Mage::helper("mpsellerbuyercomm")->__("Seller Conversation"));
            return $this;
        }
        
        public function indexAction() {
            $this->_initAction()->renderLayout();
        }

        public function gridAction()    {
            $this->loadLayout();
            $this->getResponse()->setBody(
                   $this->getLayout()->createBlock("mpsellerbuyercomm/adminhtml_selleradminqueryview_grid")->toHtml()
            );
        }

        public function newAction() {
            $this->_forward('edit');
        }

        public function editAction() { 
                $this->loadLayout();
                $this->_setActiveMenu('marketplace');

                $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
                $this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

                $this->getLayout()->getBlock("head")->setTitle(Mage::helper("mpsellerbuyercomm")->__("Reply to Seller"));

                $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

                $this->_addContent($this->getLayout()->createBlock('mpsellerbuyercomm/adminhtml_selleradminqueryview_edit'))
                   ->_addLeft($this->getLayout()->createBlock('mpsellerbuyercomm/adminhtml_selleradminqueryview_edit_tabs'));

                $this->renderLayout();
        }
     
        public function saveAction() {
            try {
                if ($data = $this->getRequest()->getPost()) {
                    $data = $this->getRequest()->getParams();
                    $model = Mage::getModel("mpsellerbuyercomm/conversation");
                    $model->setCommid($data["id"]);
                    $model->setMessage($data["message"]);
                    $model->setCreatedAt(time());
                    $model->setSender('admin');   
                    $model->setSendertype('2');   
                    $model->save();

                    $comm_coll = Mage::getModel("mpsellerbuyercomm/sellerbuyercomm")->load($data["id"]);

                    $emailTemplate = Mage::getModel('core/email_template')->loadDefault('communication_email');
                    $emailTemplateVariables = array();

                    $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
                    $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
                    $adminUsername = 'Admin';

                    $mail = Mage::getModel('customer/customer')->load($comm_coll->getSellerid());
                    $SellerName = $mail->getName();
                    $sellerEmail = $mail->getEmail();

                    $emailTemplateVariables['sendertype']   =   "Admin";
                    $emailTemplateVariables['subject']      =   $comm_coll->getSubject();
                    $emailTemplateVariables['myvar1']       =   $SellerName;
                    $emailTemplateVariables['myvar4']       =   $data["message"];
                    $emailTemplateVariables['url']          =   Mage::getUrl('mpsellerbuyercomm/seller/view/',array('id'=>$data["id"]));
                    $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
                    $emailTemplate->setSenderName($adminUsername);
                    $emailTemplate->setSenderEmail($adminEmail);
                    $emailTemplate->send($sellerEmail,$SellerName,$emailTemplateVariables);

                    Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('mpsellerbuyercomm')->__('Message has been successfully send to seller'));
                    Mage::getSingleton('adminhtml/session')->setFormData(false);
                   
                    $this->_redirect('*/*/index', array('id' => $this->getRequest()->getParam('id')));
                    return;
                }else{
                    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('mpsellerbuyercomm')->__('Unable to find item to save'));
                    $this->_redirect('*/*/index', array('id' => $this->getRequest()->getParam('id')));
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/new', array('id' => $this->getRequest()->getParam('id'),'id' => $this->getRequest()->getParam('id')));
                return;
            }
        }

        protected function _sendUploadResponse($fileName, $content, $contentType="application/octet-stream") {
            $response = $this->getResponse();
            $response->setHeader("HTTP/1.1 200 OK", "");
            $response->setHeader("Pragma", "public", true);
            $response->setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0", true);
            $response->setHeader("Content-Disposition", "attachment; filename=" . $fileName);
            $response->setHeader("Last-Modified", date("r"));
            $response->setHeader("Accept-Ranges", "bytes");
            $response->setHeader("Content-Length", strlen($content));
            $response->setHeader("Content-type", $contentType);
            $response->setBody($content);
            $response->sendResponse();
            die;
        }

        public function exportconversationCsvAction() {
            $fileName = "selleradmincoversation.csv";
            $content = $this->getLayout()->createBlock("mpsellerbuyercomm/adminhtml_selleradminqueryview_grid")->getCsv();
            $this->_sendUploadResponse($fileName, $content);
        }

        public function exportconversationXmlAction() {
            $fileName = "selleradmincoversation.xml";
            $content = $this->getLayout()->createBlock("mpsellerbuyercomm/adminhtml_selleradminqueryview_grid")->getXml();
            $this->_sendUploadResponse($fileName, $content);
        }

        public function massDeleteconversationAction() {
            $id = $this->getRequest()->getParam("id");
            $Ids = $this->getRequest()->getParam("commids");
            if(!is_array($Ids))
                Mage::getSingleton("adminhtml/session")->addError(Mage::helper("adminhtml")->__("Please select item (s)"));
            else {
                try {
                    foreach($Ids as $Id){
                        Mage::getModel("mpsellerbuyercomm/conversation")->load($Id)->delete();
                    }
                    Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Total of ").count($Ids).Mage::helper("adminhtml")->__(" record(s) were successfully deleted"));
                }
                catch(Exception $e) {
                    Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                }
            }
            $this->_redirect("*/*/index",array("id" => $id));
        }
    } 