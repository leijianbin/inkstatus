<?php
    class Webkul_Mpsellerbuyercomm_Adminhtml_SellerController extends Mage_Adminhtml_Controller_Action {

        protected function _initAction() {
            $this->loadLayout()->_setActiveMenu("mpsellerbuyercomm");
            $this->getLayout()->getBlock("head")->setTitle(Mage::helper("mpsellerbuyercomm")->__("Seller Conversation"));
            return $this;
        }
        
        public function indexAction() {
            $this->_initAction()->renderLayout();
        }

        public function gridAction()    {
            $this->loadLayout();
            $this->getResponse()->setBody(
                   $this->getLayout()->createBlock("mpsellerbuyercomm/adminhtml_selleradminquery_grid")->toHtml()
            );
        }

        public function gridviewAction()    {
            $this->loadLayout();
            $this->getResponse()->setBody(
                   $this->getLayout()->createBlock("mpsellerbuyercomm/adminhtml_selleradminqueryview_grid")->toHtml()
            );
        }

        protected function _sendUploadResponse($fileName, $content, $contentType="application/octet-stream") {
            $response = $this->getResponse();
            $response->setHeader("HTTP/1.1 200 OK", "");
            $response->setHeader("Pragma", "public", true);
            $response->setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0", true);
            $response->setHeader("Content-Disposition", "attachment; filename=" . $fileName);
            $response->setHeader("Last-Modified", date("r"));
            $response->setHeader("Accept-Ranges", "bytes");
            $response->setHeader("Content-Length", strlen($content));
            $response->setHeader("Content-type", $contentType);
            $response->setBody($content);
            $response->sendResponse();
            die;
        }

        public function exportCsvAction() {
            $fileName = "sellercommlist.csv";
            $content = $this->getLayout()->createBlock("mpsellerbuyercomm/adminhtml_selleradminquery_grid")->getCsv();
            $this->_sendUploadResponse($fileName, $content);
        }

        public function exportXmlAction() {
            $fileName = "sellercommlist.xml";
            $content = $this->getLayout()->createBlock("mpsellerbuyercomm/adminhtml_selleradminquery_grid")->getXml();
            $this->_sendUploadResponse($fileName, $content);
        }

        public function massDeletesbcommAction() {
            $Ids = $this->getRequest()->getParam("ids");
            if(!is_array($Ids))
                Mage::getSingleton("adminhtml/session")->addError(Mage::helper("adminhtml")->__("Please select item (s)"));
            else {
                try {
                    foreach($Ids as $Id){
                        Mage::getModel("mpsellerbuyercomm/sellerbuyercomm")->load($Id)->delete();
                        $coll_coversations = Mage::getModel("mpsellerbuyercomm/conversation")->getCollection()
                            ->addFieldToFilter('commid',array('eq'=>$Id));
                        foreach ($coll_coversations as $key) {
                            $key->delete();
                        }
                    }
                    Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Total of ").count($Ids).Mage::helper("adminhtml")->__(" record(s) were successfully deleted"));
                }
                catch(Exception $e) {
                    Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                }
            }
            $this->_redirect("*/*/index");
        }
        public function massDeleteconversationAction() {
            $id = $this->getRequest()->getParam("id");
            $Ids = $this->getRequest()->getParam("commids");
            if(!is_array($Ids))
                Mage::getSingleton("adminhtml/session")->addError(Mage::helper("adminhtml")->__("Please select item (s)"));
            else {
                try {
                    foreach($Ids as $Id){
                        Mage::getModel("mpsellerbuyercomm/conversation")->load($Id)->delete();
                    }
                    Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Total of ").count($Ids).Mage::helper("adminhtml")->__(" record(s) were successfully deleted"));
                }
                catch(Exception $e) {
                    Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                }
            }
            $this->_redirect("*/*/view",array("id" => $id));
        }
    } 