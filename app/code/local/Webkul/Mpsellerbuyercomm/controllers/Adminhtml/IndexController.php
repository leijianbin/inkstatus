<?php
    class Webkul_Mpsellerbuyercomm_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action {

        protected function _initAction() {
            $this->loadLayout()->_setActiveMenu("marketplace");
            $this->getLayout()->getBlock("head")->setTitle(Mage::helper("mpsellerbuyercomm")->__("Seller Buyer Conversation"));
            return $this;
        }
        
        public function indexAction() {
            $this->_initAction()->renderLayout();
        }

        public function gridAction()    {
            $this->loadLayout();
            $this->getResponse()->setBody(
                   $this->getLayout()->createBlock("mpsellerbuyercomm/adminhtml_allquery_grid")->toHtml()
            );
        }

        public function viewAction()    {
            $this->loadLayout()->_setActiveMenu('mpsellerbuyercomm')->_addBreadcrumb(Mage::helper('adminhtml')->__('Conversation'), Mage::helper('adminhtml')->__('Conversation'));      
            $this->_addContent($this->getLayout()->createBlock('mpsellerbuyercomm/adminhtml_allquery_view'));
            $this->getLayout()->getBlock('head')->setTitle($this->__('Conversation History'));      
            $this->renderLayout();
        }

        public function gridviewAction()    {
            $this->loadLayout();
            $this->getResponse()->setBody(
                   $this->getLayout()->createBlock("mpsellerbuyercomm/adminhtml_allquery_view")->toHtml()
            );
        }

        protected function _sendUploadResponse($fileName, $content, $contentType="application/octet-stream") {
            $response = $this->getResponse();
            $response->setHeader("HTTP/1.1 200 OK", "");
            $response->setHeader("Pragma", "public", true);
            $response->setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0", true);
            $response->setHeader("Content-Disposition", "attachment; filename=" . $fileName);
            $response->setHeader("Last-Modified", date("r"));
            $response->setHeader("Accept-Ranges", "bytes");
            $response->setHeader("Content-Length", strlen($content));
            $response->setHeader("Content-type", $contentType);
            $response->setBody($content);
            $response->sendResponse();
            die;
        }

        public function exportCsvAction() {
            $fileName = "sellerbuyercommlist.csv";
            $content = $this->getLayout()->createBlock("mpsellerbuyercomm/adminhtml_allquery_grid")->getCsv();
            $this->_sendUploadResponse($fileName, $content);
        }

        public function exportXmlAction() {
            $fileName = "sellerbuyercommlist.xml";
            $content = $this->getLayout()->createBlock("mpsellerbuyercomm/adminhtml_allquery_grid")->getXml();
            $this->_sendUploadResponse($fileName, $content);
        }

        public function exportconversationCsvAction() {
            $fileName = "conversation.csv";
            $content = $this->getLayout()->createBlock("mpsellerbuyercomm/adminhtml_allquery_view")->getCsv();
            $this->_sendUploadResponse($fileName, $content);
        }

        public function exportconversationXmlAction() {
            $fileName = "conversation.xml";
            $content = $this->getLayout()->createBlock("mpsellerbuyercomm/adminhtml_allquery_view")->getXml();
            $this->_sendUploadResponse($fileName, $content);
        }

        public function massDeletesbcommAction() {
            $Ids = $this->getRequest()->getParam("ids");
            if(!is_array($Ids))
                Mage::getSingleton("adminhtml/session")->addError(Mage::helper("adminhtml")->__("Please select item (s)"));
            else {
                try {
                    foreach($Ids as $Id){
                        Mage::getModel("mpsellerbuyercomm/sellerbuyercomm")->load($Id)->delete();
                        $coll_coversations = Mage::getModel("mpsellerbuyercomm/conversation")->getCollection()
                            ->addFieldToFilter('commid',array('eq'=>$Id));
                        foreach ($coll_coversations as $key) {
                            $key->delete();
                        }
                    }
                    Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Total of ").count($Ids).Mage::helper("adminhtml")->__(" record(s) were successfully deleted"));
                }
                catch(Exception $e) {
                    Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                }
            }
            $this->_redirect("*/*/index");
        }
    } 