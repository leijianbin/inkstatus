<?php
require_once "Mage/Customer/controllers/AccountController.php";
class Webkul_Mpsellerbuyercomm_AdminmsgController extends Mage_Customer_AccountController    {
    
    public function indexAction() {
        $this->loadLayout(array("default","admincomm_list"));
        $this->getLayout()->getBlock("head")->setTitle(Mage::helper("mpsellerbuyercomm")->__("Admin Message Notification"));
        $this->renderLayout();
    }
    
    protected function viewAction(){
        $id = $this->getRequest()->getParam("id");
        $sellerbuyercomm = Mage::getModel("mpsellerbuyercomm/sellerbuyercomm")->load($id);
        if($sellerbuyercomm->getSellerid() == Mage::getSingleton("customer/session")->getId()){
            $this->loadLayout(array("default","sellercomm_listview"));
            $this->getLayout()->getBlock("head")->setTitle(Mage::helper("mpsellerbuyercomm")->__("Seller Admin Communication"));
            $this->renderLayout();
        }
        else{
            Mage::getSingleton("core/session")->addError(Mage::helper("mpsellerbuyercomm")->__("Sorry You Are Not Authorised to view this Conversation"));
            $this->_redirect("*/*/");
        }
    }

    protected function saveconversationAction(){
        $sellername = Mage::getSingleton("customer/session")->getCustomer()->getName();
        $selleremail = Mage::getSingleton("customer/session")->getCustomer()->getEmail();

        $data = $this->getRequest()->getParams();        

        $comm_coll = Mage::getModel("mpsellerbuyercomm/sellerbuyercomm")->load($data["commid"]);

        if($comm_coll->getAdmintype() == 1){
            $model = Mage::getModel("mpsellerbuyercomm/conversation");
            $model->setCommid($data["commid"]);
            $model->setMessage($data["message"]);
            $model->setCreatedAt(time());
            $model->setSender($sellername);   
            $model->setSendertype($data['sendertype']);   
            $model->save();

            $emailTemplate = Mage::getModel('core/email_template')->loadDefault('communication_email');            
            $admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
            $adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
            $adminUsername = 'Admin';

            //$emailTemplate = Mage::getModel('core/email_template')->loadDefault('admincommunication_email');
            $emailTemplateVariables = array();
            $emailTemplateVariables['sendertype']   =   "Seller";
            $emailTemplateVariables['subject']      =   $comm_coll->getSubject();
            $emailTemplateVariables['myvar1']       =   $adminUsername;
            $emailTemplateVariables['myvar4']       =   $data["message"];
            $emailTemplateVariables['url']          =   Mage::helper('adminhtml')->getUrl('mpsellerbuyercomm/adminhtml_seller/view',array('id'=>$data["commid"]));
            $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
            $emailTemplate->setSenderName($sellername);
            $emailTemplate->setSenderEmail($selleremail);
            $emailTemplate->send($adminEmail,$adminUsername,$emailTemplateVariables);

            Mage::getSingleton("core/session")->addSuccess(Mage::helper("mpsellerbuyercomm")->__("Message has been send Successfully"));
        }
        $this->_redirect("*/seller/view/", array("id" => $data["commid"]));
    }
}