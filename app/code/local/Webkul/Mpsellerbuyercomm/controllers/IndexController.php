<?php
require_once "Mage/Customer/controllers/AccountController.php";
class Webkul_Mpsellerbuyercomm_IndexController extends Mage_Customer_AccountController	{
	
	public function indexAction() {			
		$this->loadLayout(array("default","buyercomm_list"));
		$this->getLayout()->getBlock("head")->setTitle(Mage::helper("mpsellerbuyercomm")->__("Seller Buyer Communication"));
		$this->renderLayout();
	}

	protected function viewAction(){
    	$id = $this->getRequest()->getParam("id");
    	$mpsellerbuyercomm = Mage::getModel("mpsellerbuyercomm/sellerbuyercomm")->load($id);
    	if($mpsellerbuyercomm->getCustomerid() == Mage::getSingleton("customer/session")->getId()){
    		$this->loadLayout(array("default","buyercomm_listview"));
			$this->getLayout()->getBlock("head")->setTitle(Mage::helper("mpsellerbuyercomm")->__("Buyer Seller Communication"));
			$this->renderLayout();
    	}
    	else{
    		Mage::getSingleton("core/session")->addError(Mage::helper("mpsellerbuyercomm")->__("Sorry You Are Not Authorised to view this Conversation"));
	    	$this->_redirect("*/*/");
    	}
    }

    protected function saveconversationAction(){
    	$buyername = Mage::getSingleton("customer/session")->getCustomer()->getName();
    	$buyeremail = Mage::getSingleton("customer/session")->getCustomer()->getEmail();

    	$data = $this->getRequest()->getParams();
    	$model = Mage::getModel("mpsellerbuyercomm/conversation");
    	$model->setCommid($data["commid"]);
    	$model->setMessage($data["message"]);
    	$model->setCreatedAt(time());
    	$model->setSender(Mage::getSingleton("customer/session")->getCustomer()->getName());   
    	$model->setSendertype($data['sendertype']);   
    	$model->save();

		$comm_coll = Mage::getModel("mpsellerbuyercomm/sellerbuyercomm")->load($data["commid"]);

        $emailTemplate = Mage::getModel('core/email_template')->loadDefault('communication_email');
        $emailTemplateVariables = array();

        $mail = Mage::getModel('customer/customer')->load($comm_coll->getSellerid());
        $SellerName = $mail->getName();
        $sellerEmail = $mail->getEmail();

        $emailTemplateVariables['sendertype'] 	= 	"Customer";
        $emailTemplateVariables['subject'] 		= 	$comm_coll->getSubject();
        $emailTemplateVariables['myvar1']  		= 	$SellerName;
        $emailTemplateVariables['myvar4']  		= 	$data["message"];
		$emailTemplateVariables['url'] 			=	Mage::getUrl('mpsellerbuyercomm/seller/view/',array('id'=>$data["commid"]));
		$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
		$emailTemplate->setSenderName($buyername);
		$emailTemplate->setSenderEmail($buyeremail);
		$emailTemplate->send($sellerEmail,$SellerName,$emailTemplateVariables);

		if(Mage::getStoreConfig('mpsellerbuyercomm/admin/notification')==1){
			$adminemail = Mage::getStoreConfig("trans_email/ident_general/email");
        	$adminname = Mage::getStoreConfig("trans_email/ident_general/name");

			$emailTemplate = Mage::getModel('core/email_template')->loadDefault('admincommunication_email');
	        $emailTemplateVariables = array();

	        $mail = Mage::getModel('customer/customer')->load($comm_coll->getSellerid());
	        $SellerName = $mail->getName();
	        $sellerEmail = $mail->getEmail();

	        $emailTemplateVariables['sendertype'] 	= 	"Customer to Seller";
	        $emailTemplateVariables['subject'] 		= 	$comm_coll->getSubject();
	        $emailTemplateVariables['myvar1']  		= 	$SellerName;
	        $emailTemplateVariables['myvar4']  		= 	$data["message"];
	        $emailTemplateVariables['url'] 			=	Mage::helper('adminhtml')->getUrl('mpsellerbuyercomm/adminhtml_index/view',array('id'=>$data["commid"]));
			$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
			$emailTemplate->setSenderName($buyername);
			$emailTemplate->setSenderEmail($buyeremail);
			$emailTemplate->send($adminemail,$adminname,$emailTemplateVariables);
		}

    	Mage::getSingleton("core/session")->addSuccess(Mage::helper("mpsellerbuyercomm")->__("Message Saved Successfully"));
    	$this->_redirect("*/index/view/", array("id" => $data["commid"]));
    }
}