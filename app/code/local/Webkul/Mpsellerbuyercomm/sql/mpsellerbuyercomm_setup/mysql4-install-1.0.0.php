<?php
$installer = $this;
$installer->startSetup();
$installer->run("

CREATE TABLE IF NOT EXISTS `{$this->getTable('wk_mpsellerbuyercomm')}` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `productid` int(11) NOT NULL,
  `sellerid` int(11) NOT NULL,
  `customerid` int(11) NOT NULL,
  `admintype` int(2) NOT NULL DEFAULT '0',
  `subject` text NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `{$this->getTable('wk_mpsellerbuyercomm_conversation')}` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `commid` int(11) NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL,
  `sender` varchar(50) NOT NULL,
  `sendertype` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");

$installer->endSetup();