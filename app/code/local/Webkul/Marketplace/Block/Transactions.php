<?php
class Webkul_Marketplace_Block_Transactions extends Mage_Core_Block_Template
{
	protected $_productsCollection = null;
	public function __construct(){		
		parent::__construct();	
    	$id = Mage::getSingleton('customer/session')->getId();
	   $collection = Mage::getModel('marketplace/saleslist')->getCollection();
       $collection->addFieldToFilter('mageproownerid',array('eq'=>$id));
         $collection->addFieldToFilter('cpprostatus',array('eq'=>!0));
	   $collection->setOrder('autoid');
		$this->setCollection($collection);
	}
	 protected function _prepareLayout() {
        parent::_prepareLayout(); 
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,'all'=>'all'));
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    } 
	
    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }
}
