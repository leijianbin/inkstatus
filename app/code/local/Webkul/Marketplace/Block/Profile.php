<?php
class Webkul_Marketplace_Block_Profile extends Mage_Core_Block_Template
{
	public function _prepareLayout(){
		// $partner=$this->getProfileDetail();
		// if($partner->getShoptitle()!='')
		// 	$this->getLayout()->getBlock('head')->setTitle($partner->getShoptitle());
		// else
		// 	$this->getLayout()->getBlock('head')->setTitle($partner->getProfileurl());
		// $this->getLayout()->getBlock('head')->setKeywords($partner->getMetaKeyword());	
		// $this->getLayout()->getBlock('head')->setDescription($partner->getMetaDescription());

        $toolbar = $this->getToolbarBlock();
        $collection = $this->getCollection();
		
        if ($orders = $this->getAvailableOrders()) {
           $toolbar->setAvailableOrders($orders);
        }
        if ($sort = $this->getSortBy()) {
            $toolbar->setDefaultOrder($sort);
        }
        if ($dir = $this->getDefaultDirection()) {
            $toolbar->setDefaultDirection($dir);
        }
        $toolbar->setCollection($collection);
 
        $this->setChild('toolbar', $toolbar);
        $this->getCollection()->load();

		$partner=$this->getProfileDetail();

		if($partner->getShoptitle()!='')
			$this->getLayout()->getBlock('head')->setTitle($partner->getShoptitle());
		else
			$this->getLayout()->getBlock('head')->setTitle($partner->getProfileurl());
		$this->getLayout()->getBlock('head')->setKeywords($partner->getMetaKeyword());		
		$this->getLayout()->getBlock('head')->setDescription($partner->getMetaDescription());
        return $this;

		return parent::_prepareLayout();
    }
    
	public function getProfileDetail(){
		$temp=explode('/profile',Mage::helper('core/url')->getCurrentUrl());
		$temp=explode('/',$temp[1]); //be careful about the parse 
		if($temp[1]!=''){
			$temp1 = explode('?', $temp[1]);
			$data=Mage::getModel('marketplace/userprofile')->getCollection()
						->addFieldToFilter('mageuserid',array('eq'=>$temp1[0]));
			foreach($data as $seller){ return $seller;}
		}
	}

	// public function getProfileDetail(){
	// 	$temp=explode('/collection',Mage::helper('core/url')->getCurrentUrl());
	// 	$temp=explode('/',$temp[1]);
	// 	if($temp[1]!=''){
 	//      $temp1 = explode('?', $temp[1]);
	// 		$data=Mage::getModel('marketplace/userprofile')->getCollection()
	// 					->addFieldToFilter('profileurl',array('eq'=>$temp1[0]));
	// 		foreach($data as $seller){ return $seller;}
	// 	}
	// }
	
	public function getFeed(){
		$temp=explode('/profile/',Mage::helper('core/url')->getCurrentUrl());
		if($temp[1]!=''){
			$temp1 = explode('?', $temp[1]);
			$data=Mage::getModel('marketplace/userprofile')->getCollection()
						->addFieldToFilter('profileurl',array('eq'=>$temp1[0]));
			foreach($data as $seller){ $id=$seller->getMageuserid();}
		}
		return Mage::getModel('marketplace/feedback')->getTotal($id);
	}
	
	public function getBestsellProducts(){
		$temp=explode('/profile/',Mage::helper('core/url')->getCurrentUrl());
		$products=array();
		if($temp[1]!=''){
			$temp1 = explode('?', $temp[1]);
			$data=Mage::getModel('marketplace/userprofile')->getCollection()
						->addFieldToFilter('profileurl',array('eq'=>$temp1[0]));
			foreach($data as $seller){ $id=$seller->getmageuserid();}
			$data=Mage::getModel('marketplace/product')->getCollection()
									->addFieldToFilter('userid',array('eq'=>$id))
									->addFieldToFilter('status',array('neq'=>2))->setPageSize(9)
									->setOrder('mageproductid', 'DESC');
		   foreach($data as $data1){
				array_push($products,$data1->getMageproductid());
			}		
		}
		return $products;
	}
    /* public function getCustomerpartner(){ 
        if (!$this->hasData('marketplace')) {
            $this->setData('marketplace', Mage::registry('marketplace'));
        } 
    }*/

    public function __construct(){		
		parent::__construct();
		if(array_key_exists('c', $_GET)){	
    	$cate = Mage::getModel('catalog/category')->load($_GET["c"]);
	    }	
    	$partner=$this->getProfileDetail();
    	//print_r($partner);
        $productname=$this->getRequest()->getParam('name');
		$querydata = Mage::getModel('marketplace/product')->getCollection()
				->addFieldToFilter('userid', array('eq' => $partner->getmageuserid()))
				->addFieldToFilter('status', array('neq' => 2))
				->setOrder('mageproductid');
		$rowdata=array();		
		foreach ($querydata as  $value) {
			$qty = (int)Mage::getModel('cataloginventory/stock_item')
                ->loadByProduct($value->getMageproductid())->getQty();
            if($qty){
				$rowdata[] = $value->getMageproductid();
            }
		}
		$collection = Mage::getModel('catalog/product')->getCollection();
		$collection->addAttributeToSelect('*');
		
		if(array_key_exists('c', $_GET)){
			$collection->addCategoryFilter($cate);
		}
            $collection->addAttributeToFilter('entity_id', array('in' => $rowdata));
			if((Mage::helper('core')->isModuleEnabled('Webkul_Webkulsearch')) && ($productname!='')){
                $collection->addFieldToFilter('name', array('like' => '%'.$productname.'%'));   
            }
    		$this->setCollection($collection);	
    	}
	
    public function getDefaultDirection(){
        return 'asc';
    }
    public function getAvailableOrders(){
        return array('price'=>'Price','name'=>'Name');
    }
    public function getSortBy(){
        return 'collection_id';
    }
     public function getToolbarBlock(){
       $block = $this->getLayout()->createBlock('marketplace/toolbar', microtime());
        return $block;
    }
    /*public function getToolbarBlock()
    {
       if ($blockName = $this->getToolbarBlockName()) {
            if ($block = $this->getLayout()->getBlock($blockName)) {
                return $block;
            }
        }
        $block = $this->getLayout()->createBlock($this->_defaultToolbarBlock, microtime());
        return $block;
    }*/
    public function getMode()
    {
        return $this->getChild('toolbar')->getCurrentMode();
    }
 
    public function getToolbarHtml()   {
        return $this->getChildHtml('toolbar');
    }
	public function getColumnCount() {
		return 4;
    } 
}