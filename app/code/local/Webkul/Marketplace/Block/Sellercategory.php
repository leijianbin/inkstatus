<?php
class Webkul_Marketplace_Block_Sellercategory extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
	
	public function getCategoryList(){
		$sellerid=$this->getProfileDetail()->getmageuserid();
		$products=Mage::getModel('marketplace/product')->getCollection()
								->addFieldToFilter('userid',array('eq'=>$sellerid))
								->addFieldToFilter('status', array('neq' => 2));
		$catgories=array();
		foreach($products as $product){
			$qty = (int)Mage::getModel('cataloginventory/stock_item')
                        ->loadByProduct($product->getMageproductid())->getQty();
            if($qty){                   
				$catproduct = Mage::getModel('catalog/product')
										->load($product->getMageproductid());
				if($catproduct->getStatus()==1){
					$cats = $catproduct->getCategoryCollection()->addAttributeToSelect('id');
					$temp=array();
					foreach($cats as $cat){
						$_category = Mage::getModel('catalog/category')->load($cat->getId());
						if ($_category->getIsActive()){
							array_push($temp,$cat->getId());
						}
					}                       
					$catgories= array_merge($catgories,$temp);
				}       
			}       
		}
		return array_count_values($catgories);
	}
	
	public function getProfileDetail(){
		$temp=explode('/collection',Mage::helper('core/url')->getCurrentUrl());
		$temp=explode('/',$temp[1]);
		if($temp[1]!=''){
			$temp1 = explode('?', $temp[1]);
			$data=Mage::getModel('marketplace/userprofile')->getCollection()
						->addFieldToFilter('profileurl',array('eq'=>$temp1[0]));
			foreach($data as $seller){ return $seller;}
		}
	}
    
}