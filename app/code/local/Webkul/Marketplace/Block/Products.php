<?php

class Webkul_Marketplace_Block_Products extends Mage_Core_Block_Template
{
    protected $_productsCollection = null;
    public function __construct(){      
        parent::__construct();
        $ids = array();
        $userid = Mage::getSingleton('customer/session')->getId();
        $collection_orders = Mage::getModel('marketplace/saleslist')->getCollection()
                                ->addFieldToFilter('mageproownerid',array('eq'=>$userid))
                                ->addFieldToSelect('mageorderid')
                                ->distinct(true);
        foreach ($collection_orders as $collection_order) {
            $collection_ids = Mage::getModel('marketplace/saleslist')->getCollection()
                            ->addFieldToFilter('mageorderid',array('eq'=>$collection_order->getMageorderid()))
                            ->setOrder('autoid','DESC')
                            ->setPageSize(1);
            foreach ($collection_ids as $collection_id) {
                $autoid = $collection_id->getAutoid();
            }
            array_push($ids, $autoid);
        }
        

        $collection = Mage::getModel('marketplace/saleslist')->getCollection();
        $collection->addFieldToFilter('autoid',array('in'=>$ids));
        $this->setCollection($collection);
    }
     protected function _prepareLayout() {
        parent::_prepareLayout(); 
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,'all'=>'all'));
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    } 
    
    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }
}
