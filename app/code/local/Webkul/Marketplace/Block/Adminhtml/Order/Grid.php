<?php
class Webkul_Marketplace_Block_Adminhtml_Order_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('marketplaceGrid');
	  $this->setUseAjax(true);
      $this->setDefaultSort('autoid');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {		
		$customerid=$this->getRequest()->getParam('id');
		$collection = Mage::getModel('marketplace/saleslist')->getCollection();
    $collection->addFieldToFilter('mageproownerid',array('eq'=>$customerid));
		$collection->addFieldToFilter('magerealorderid',array('neq'=>0));
		foreach ($collection as $item) {
			$item->view='<a href="'.$this->getUrl('adminhtml/sales_order/view/order_id/'.$item->getMageorderid()).'">view</a>';
			$order = Mage::getModel('sales/order')->load($item->getMageorderid());
			$status=$order->getStatus();
			$item->status=$status;
		}
		$this->setCollection($collection);
		return parent::_prepareCollection();
  }

  protected function _prepareColumns(){
       $this->addColumn('mageorderid', array(
            'header'    => Mage::helper('marketplace')->__('ID'),
            'width'     => '50px',
            'index'     => 'mageorderid',
            'type'  => 'number',
			'filter'    => false,
            'sortable'  => false
        ));
        $this->addColumn('magerealorderid', array(
            'header'    => Mage::helper('marketplace')->__('Order#'),
            'index'     => 'magerealorderid',
			'filter'    => false,
            'sortable'  => false
        ));
		 
		 $this->addColumn('mageproname', array(
            'header'    => Mage::helper('marketplace')->__('Product Name'),
            'index'     => 'mageproname',
			'filter'    => false,
            'sortable'  => false
        ));
		
		$this->addColumn('mageproprice', array(
            'header'    => Mage::helper('marketplace')->__('Price'),
            'index'     => 'mageproprice',
			'filter'    => false,
            'sortable'  => false
        ));
		
		$this->addColumn('status', array(
            'header'    => Mage::helper('marketplace')->__('Status'),
            'index'     => 'status',
			'filter'    => false,
            'sortable'  => false
        ));
		$this->addColumn('cleared_at', array(
            'header'    => Mage::helper('marketplace')->__('Order At'),
            'index'     => 'cleared_at',
			'filter'    => false,
            'sortable'  => false
        ));
		$this->addColumn('view', array(
            'header'    => Mage::helper('customer')->__('View'),
            'index'     => 'view',
			'type'      => 'text',
			'filter'    => false,
            'sortable'  => false
        ));
        return parent::_prepareColumns();
  }

    protected function _prepareMassaction(){
    }
	public function getGridUrl(){
		return $this->getUrl("*/*/grid",array("_current"=>true));
	}
	public function getRowUrl($row) {
        return '#';
    }
}