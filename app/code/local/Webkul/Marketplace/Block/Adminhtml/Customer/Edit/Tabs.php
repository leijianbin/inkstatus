<?php
class Webkul_Marketplace_Block_Adminhtml_Customer_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct(){
        parent::__construct();
        $this->setId('customer_info_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('customer')->__('Customer Information'));
    }

    protected function _beforeToHtml(){
        $this->addTab('account', array(
            'label'     => Mage::helper('customer')->__('Account Information'),
            'content'   => $this->getLayout()->createBlock('adminhtml/customer_edit_tab_account')->initForm()->toHtml(),
            'active'    => Mage::registry('current_customer')->getId() ? false : true
        ));
        $this->addTab('addresses', array(
            'label'     => Mage::helper('customer')->__('Addresses'),
            'content'   => $this->getLayout()->createBlock('adminhtml/customer_edit_tab_addresses')->initForm()->toHtml(),
        ));
        if (Mage::registry('current_customer')->getId()) {
            if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
                $this->addTab('orders', array(
                    'label'     => Mage::helper('customer')->__('Orders'),
                    'class'     => 'ajax',
                    'url'       => $this->getUrl('*/*/orders', array('_current' => true)),
                 ));
            }
            $this->addTab('cart', array(
                'label'     => Mage::helper('customer')->__('Shopping Cart'),
                'class'     => 'ajax',
                'url'       => $this->getUrl('*/*/carts', array('_current' => true)),
            ));
            $this->addTab('wishlist', array(
                'label'     => Mage::helper('customer')->__('Wishlist'),
                'class'     => 'ajax',
                'url'       => $this->getUrl('*/*/wishlist', array('_current' => true)),
            ));
            if (Mage::getSingleton('admin/session')->isAllowed('newsletter/subscriber')) {
                $this->addTab('newsletter', array(
                    'label'     => Mage::helper('customer')->__('Newsletter'),
                    'content'   => $this->getLayout()->createBlock('adminhtml/customer_edit_tab_newsletter')->initForm()->toHtml()
                ));
            }
            if (Mage::getSingleton('admin/session')->isAllowed('catalog/reviews_ratings')) {
                $this->addTab('reviews', array(
                    'label'     => Mage::helper('customer')->__('Product Reviews'),
                    'class'     => 'ajax',
                    'url'       => $this->getUrl('*/*/productReviews', array('_current' => true)),
                ));
            }
            if (Mage::getSingleton('admin/session')->isAllowed('catalog/tag')) {
                $this->addTab('tags', array(
                    'label'     => Mage::helper('customer')->__('Product Tags'),
                    'class'     => 'ajax',
                    'url'       => $this->getUrl('*/*/productTags', array('_current' => true)),
                ));
            }
			$customerid=Mage::registry('current_customer')->getId();
			$isPartner= Mage::getModel('marketplace/userprofile')->isPartner();
			if($isPartner==1){
				$this->addTab('payment', array(
					'label'     => Mage::helper('customer')->__('Payment Mode'),
					'content'   => $this->paymentmode(),
				));
				$this->addTab('sellercommision', array(
					'label'     => Mage::helper('customer')->__("Seller's Commision"),
					'content'   => $this->sellercommision(),
				));	
				$this->addTab('newproduct', array(
					'label'     => Mage::helper('customer')->__("Add Product"),
					'content'   => $this->addproduct(),
				));
				$this->addTab('notpartner', array(
					'label'     => Mage::helper('customer')->__("Want to remove a Seller"),
					'content'   => $this->removepartner(),
				));
			}
			else {
			$this->addTab('partner', array(
					'label'     => Mage::helper('customer')->__('Want to be Seller'),
					'content'   => $this->wantpartner(),
				));
			}
			
        }
        $this->_updateActiveTab();
        Varien_Profiler::stop('customer/tabs');
        return parent::_beforeToHtml();
    }	
	protected function paymentmode(){	
		$row =Mage::getModel('marketplace/userprofile')->getpaymentmode();
		if($row!=''){	
			return '<div class="entry-edit">
						<div class="entry-edit-head">
							<h4 class="icon-head head-customer-view">Payment Details</h4>
						</div>
						<fieldset>
							<address>
								<strong>'.$row.'</strong><br>
							</address>
						</fieldset>
					</div>';
		}
		else{
			return '<div class="entry-edit">
							<div class="entry-edit-head">
								<h4 class="icon-head head-customer-view">Payment Details</h4>
							</div>
							<fieldset>
								<address>
									<strong>Not Mentioned Yet</strong><br>
								</address>
							</fieldset>
						</div>';
		}
	
	}	
	protected function sellercommision(){
		$id=Mage::registry('current_customer')->getId();	
		$collection = Mage::getModel('marketplace/saleperpartner')->getCollection();
		$collection->addFieldToFilter('mageuserid',array('eq'=>$id));	
		if(count($collection)) {
		  foreach ($collection as $value) {
				$rowcom = $value->getCommision();
			}	
		}
		else
		{
		  $rowcom =Mage::getStoreConfig('marketplace/marketplace_options/percent'); 
		}
		$tsale=0;
		$tcomm=0;
		$tact=0;
		$collection1 = Mage::getModel('marketplace/saleslist')->getCollection();
		$collection1->addFieldToFilter('mageproownerid',array($id));	
		foreach ($collection1 as $key) {
				$tsale+=$key->gettotalamount();
				$tcomm+=$key->gettotalcommision();
				$tact+=$key->getactualparterprocost();
			}		
	
	//	if($rowcom>0) { $comm = $rowcom; } elseif(Mage::getStoreConfig('marketplace/marketplace_options/percent')>0) { $comm=Mage::getStoreConfig('marketplace/marketplace_options/percent');} else { $comm = 20;}
	    $comm = $rowcom;
		return '<div class="entry-edit">
					<div class="entry-edit-head">
						<h4 class="icon-head head-customer-view">Commision Details</h4>
					</div>
					<fieldset>
						Set Commision In Percentage For This Particular Seller : <input name="commision" type="text" classs="input-text no-changes" value="'.$rowcom.'"/>
						<table class="grid table" id="customer_cart_grid1_table" >
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>Total Sale</td>
								<td>'.Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().' '.$tsale.'</td>
							</tr>
							<tr>
								<td>Total Seller Sale</td>
								<td>'.Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().' '.$tact.'</td>
							</tr>
							<tr>
								<td>Total Admin Sale</td>
								<td>'.Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol().' '.$tcomm.'</td>
							</tr>
							<tr>
								<td>Current Commision %</td>
								<td>'.$comm.'%</td>
							</tr>
						</table>
					</fieldset>							
				</div>';
	}
	protected function addproduct(){
		$html='<div class="entry-edit">
					<div class="entry-edit-head">
						<h4 class="icon-head head-customer-view">Assign Product To Seller</h4>
					</div>
					<fieldset>
						Enter Product ID : <input name="sellerassignproid" type="text" classs="input-text no-changes" value=""/>	&nbsp;&nbsp;&nbsp;&nbsp;<b>Notice: Enter Only Integer value.</b>';
		/*$ispartnerEnabled = Mage::helper('core/data')->isModuleOutputEnabled('Webkul_Mpmassuplaodaddons');
		if($ispartnerEnabled == 1){	
			$html.='</br></br>'.Mage::helper('mpmassuplaodaddons')->__("CSV File ").':&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="file" id="csvfile" name="csvfile"/><a href="'.Mage::getBaseUrl("media").'marketplace/massuploaded/sample/mpmassproductuploadadmin.csv">Sample CSV File</a></br>'.Mage::helper('mpmassuplaodaddons')->__("Images Zip File ").':&nbsp;&nbsp;<input type="file" name="imagefiles" id="imagefiles"/>
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
				<script>
					var $wk_jq= jQuery.noConflict();
					$wk_jq(function(){
						$wk_jq("#csvfile").change(function(){
							if($wk_jq(this).val().split(".")[1]!="csv"){
								alert("upload only CSV file");
								$wk_jq(this).val("");	
							}
						});
						$wk_jq("#imagefiles").change(function(){
							if($wk_jq(this).val().split(".")[1]!="zip"){
								alert("upload only Zip file");
								$wk_jq(this).val("");	
							}
						});
					});
				</script>';
		}*/
		$html.= '</fieldset></div>';
		return $html;
	}
	
	protected function wantpartner(){	
		$customerId=Mage::registry('current_customer')->getId();
		$coll=Mage::getModel('marketplace/userprofile')->getCollection();
		$coll->addFieldToFilter('mageuserid',array('eq'=>$customerId));
		$profileurl='';
		foreach($coll as $row){
			$profileurl=$row->getProfileurl();
		}
		return '<div class="entry-edit">
					<div class="entry-edit-head">
						<h4 class="icon-head head-customer-view">Want to become a Seller</h4>
					</div>
					<fieldset>
						<input type="checkbox" name="partnertype" value="1">&nbsp;Is Seller<br><br>'.
						$this->__('Shop Name : ') .'<input type="text" name="profileurl" placeholder="Enter Shop Name" value="'.$profileurl.'" class="profileurl"/>
					</fieldset>							
				</div>
				<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
				<script>
					var $wk_jq= jQuery.noConflict();
					$wk_jq(function(){
						$wk_jq(".profileurl").keyup(function(){
							$wk_jq(this).val($wk_jq(this).val().replace(/[^a-z^0-9\.]/g,""));
						});
					});
				</script>';
	}
	
	protected function removepartner(){	
		return '<div class="entry-edit">
					<div class="entry-edit-head">
						<h4 class="icon-head head-customer-view">Want to remove Seller</h4>
					</div>
					<fieldset>
						<input type="checkbox" name="partnertype" value="2">&nbsp;Is not Seller
					</fieldset>							
				</div>';
	}
    protected function _updateActiveTab(){
        $tabId = $this->getRequest()->getParam('tab');
        if( $tabId ) {
            $tabId = preg_replace("#{$this->getId()}_#", '', $tabId);
            if($tabId) {
                $this->setActiveTab($tabId);
            }
        }
    }
}
