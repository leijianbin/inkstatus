<?php
class Webkul_Marketplace_Block_Marketplace extends Mage_Customer_Block_Account_Dashboard
{
	protected $_productsCollection = null;
	public function __construct(){		
		parent::__construct();	
    	$userId=Mage::getSingleton('customer/session')->getCustomer()->getId();
		$collection = Mage::getModel('marketplace/product')->getCollection()->addFieldToFilter('userid',array('eq'=>$userId));
		$products=array();
		foreach($collection as $data){
			array_push($products,$data->getMageproductid());
		}
		$collection = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect('*')->addFieldToFilter('entity_id',array('in'=>$products))->addAttributeToSort('created_at', 'desc');
		$this->setCollection($collection);
	}
	 protected function _prepareLayout() {
        parent::_prepareLayout(); 
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $pager->setAvailableLimit(array(5=>5,10=>10,20=>20,'all'=>'all'));
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    } 
	
    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }
	public function getProduct() {
		$id = $this->getRequest()->getParam('id');
		$products = Mage::getModel('catalog/product')->load($id);
		return $products;
	}
}
