<?php
class Webkul_Marketplace_Block_Associate extends Mage_Customer_Block_Account_Dashboard
{
	public function __construct(){		
		parent::__construct();	
	}
	
	public function getConfigurableProduct() {
		$id = $this->getRequest()->getParam('id');
		$products = Mage::getModel('catalog/product')->load($id);
		return $products;
	}
	public function getAttributeOptions($attributecode){
		$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product',$attributecode);
		$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
		return $attribute->getSource()->getAllOptions();
	}
}
