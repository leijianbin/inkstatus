<?php

class Webkul_Marketplace_Model_Feedback extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('marketplace/feedback');
    }
	public function getTotal($partnerId){
		$data=array();
		if($partnerId > 0){
			$collection = Mage::getModel('marketplace/feedback')->getCollection();
			$collection->addFieldToFilter('proownerid',array('eq'=>$partnerId));
			$collection->addFieldToFilter('status',array('neq'=>0));
			$price=0;
			$value=0;
			$quality=0;
			$totalfeed=0;
			foreach($collection as $record) {
				$price+=$record->getfeedprice();
				$value+=$record->getfeedvalue();
				$quality+=$record->getfeedquality();
			}
			if(count($collection)!=0)
			{
			$totalfeed=ceil(($price+$value+$quality)/(3*count($collection)));
		    }
		    
			$data=array('totalfeed'=>$totalfeed, 'feedcount'=>count($collection));
			return $data;
		}
	}
	public function saveFeedbackdetail($wholedata)
	{	
		$customerid=Mage::getSingleton('customer/session')->getCustomerId();
		$mail = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
		$wholedata['userid']=$customerid;
		$wholedata['useremail']=$mail; 
		$wholedata['createdat']=date("Y-m-d H:i:s", Mage::getModel('core/date')->timestamp(time()));
		$collection=Mage::getModel('marketplace/feedback');
		$collection->setData($wholedata);
		$collection->save();
		return 0;
	}
	/*public function getFeedList($partnerId){
		$data=array();
		if($partnerId > 0){
			$collection = Mage::getModel('marketplace/feedback')->getCollection()
															   ->addFieldToFilter('status',array('neq'=>0))
															   ->addFieldToFilter('proownerid',array('eq'=>$partnerId));
			$i=0;
			foreach($collection as $record) {
				$data[$i]=array(
								'userid'=>$record->getuserid(),
								'feedprice'=>$record->getfeedprice(),
								'feedvalue'=>$record->getfeedvalue(),
								'feedquality'=>$record->getfeedquality(),
								'feedreview'=>$record->getfeedreview(),
								'createdat'=>$record->getcreatedat()
								);
				$i++;
			}
			return $data;
		}
	
	}*/
}
