<?php
class Webkul_Marketplace_Model_Userprofile extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('marketplace/userprofile');
    }
	
	public function getRegisterDetail($customer)
	{
        $data=Mage::getSingleton('core/app')->getRequest();
		$wholedata=$data->getParams();
		if($wholedata['wantpartner']==1){
			$status=Mage::getStoreConfig('marketplace/marketplace_options/partner_approval')? 0:1;
			$assinstatus=Mage::getStoreConfig('marketplace/marketplace_options/partner_approval')? "Pending":"Seller";
			$customerid=$customer->getId();
			$collection=Mage::getModel('marketplace/userprofile');
			$collection->setwantpartner($status);
			$collection->setpartnerstatus($assinstatus);
			$collection->setmageuserid($customerid);
			$collection->setProfileurl($wholedata['profileurl']);
			$collection->save();
		}
	}
	
	public function massispartner($data){
		$wholedata=$data->getParams();
		foreach($wholedata['customer'] as $key){
			$collection = Mage::getModel('marketplace/userprofile')->getCollection();
			$collection->getSelect()->where('mageuserid ='.$key);
				foreach ($collection as $row) {
						$auto=$row->getautoid();
						$collection1 = Mage::getModel('marketplace/userprofile')->load($auto);
						$collection1->setwantpartner(1);
						$collection1->setpartnerstatus('Seller');
						$collection1->save();
				}
			$customer = Mage::getModel('customer/customer')->load($key);	
			$emailTemp = Mage::getModel('core/email_template')->loadDefault('partnerapprove');
			
			$emailTempVariables = array();				
			$adminEmail=Mage::getStoreConfig('trans_email/ident_general/email');
			$adminUsername = 'Admin';
			$emailTempVariables['myvar1'] = $customer->getName();
			$emailTempVariables['myvar2'] = Mage::helper('customer')->getLoginUrl();
			
			$processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
			
			$emailTemp->setSenderName($adminUsername);
			$emailTemp->setSenderEmail($adminEmail);
			$emailTemp->send($customer->getEmail(),$adminUsername,$emailTempVariables);	
		}		
	}

	public function massisnotpartner($data){ 
		$wholedata=$data->getParams();
		foreach($wholedata['customer'] as $key){
			$collection = Mage::getModel('marketplace/userprofile')->getCollection();
			$collection->getSelect()->where('mageuserid ='.$key);
				foreach ($collection as $row) {
						$auto=$row->getautoid();
						$collection1 = Mage::getModel('marketplace/userprofile')->load($auto);
						$collection1->setwantpartner(0);
						$collection1->setpartnerstatus('Default User');
						$collection1->save();
				}
			$customer = Mage::getModel('customer/customer')->load($key);	
			$emailTemp = Mage::getModel('core/email_template')->loadDefault('partnerdisapprove');
			$emailTempVariables = array();				
			$adminEmail=Mage::getStoreConfig('trans_email/ident_general/email');
			$adminUsername = 'Admin';
			$emailTempVariables['myvar1'] = $customer->getName();
			$emailTempVariables['myvar2'] = Mage::helper('customer')->getLoginUrl();
			
			$processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
			
			$emailTemp->setSenderName($adminUsername);
			$emailTemp->setSenderEmail($adminEmail);
			$emailTemp->send($customer->getEmail(),$adminUsername,$emailTempVariables);
		}		
	}	
	
	public function getPartnerProfileById($partnerId) {
        $data = array();
        if ($partnerId != '') {
            $collection = Mage::getModel('marketplace/userprofile')->getCollection();
            $collection->addFieldToFilter('mageuserid',array('eq'=>$partnerId));
			$user = Mage::getModel('customer/customer')->load($partnerId);
			$name=explode(' ',$user->getName());
			foreach ($collection as $record) {
				$banner3pic=$record->getbanner3pic();
				$bannerpic=$record->getbannerpic();
				$logopic=$record->getlogopic();
				$countrylogopic=$record->getcountrypic();
				if(strlen($bannerpic)<=0){$bannerpic='banner-image.png';}
				if(strlen($banner3pic)<=0){$banner3pic='banner-image.png';}
				if(strlen($logopic)<=0){$logopic='noimage.png';}
				if(strlen($countrylogopic)<=0){$countrylogopic='';}
				$data= array(
						'firstname'=>$name[0],
						'lastname'=>$name[1],
						'email'=>$user->getEmail(),
						'twitterid'=>$record->gettwitterid(),
						'facebookid'=>$record->getfacebookid(),
						'pin_id'=>$record->getpin_id(),
						'tumblrid'=>$record->gettumblrid(),
						'profileurl'=>$record->getprofileurl(),
						'mageuserid'=>$record->getmageuserid(), 
						'bannerpic'=>$bannerpic,
						'banner3pic'=>$banner3pic,
						'logopic'=>$logopic,
						'countrypic'=>$countrylogopic,
						'comstate'=>$record->getcomstate(),
						'complocality'=>$record->getcomplocality(),
						'meta_keyword'=>$record->getMetaKeyword(),					
						'meta_description'=>$record->getMetaDescription(),
						'compdesi'=>$record->getcompdesi(),
						'profileurl'=>$record->getProfileurl(),
						'shoptitle'=>$record->getShoptitle(),
						'backgroundth'=>$record->getbackgroundth(),
						'wantpartner'=>$record->getwantpartner()
					);
			}
			return $data;
		}
    } 
	
	public function isPartner(){
		$partnerId=Mage::getSingleton('customer/session')->getCustomerId();
		if($partnerId=='')
			$partnerId=Mage::registry('current_customer')->getId();
		if ($partnerId != '') {
			$data=0;
			$collection = Mage::getModel('marketplace/userprofile')->getCollection();
            $collection->addFieldToFilter('mageuserid',array('eq'=>$partnerId));
			foreach ($collection as $record) {
				$data=$record->getwantpartner();
			}
			return $data;
		}
	}
	public function isRightSeller($productid){
		$customerid=Mage::getSingleton('customer/session')->getCustomerId();
		$data=0;
		$product=Mage::getModel('marketplace/product')->getCollection()
													 ->addFieldToFilter('userid',array('eq'=>$customerid))
												     ->addFieldToFilter('mageproductid',array('eq'=>$productid));
		foreach ($product as $record){
				$data=1;
		}
		return $data;											 
	}
	public function getpaymentmode(){
		$partnerId=Mage::registry('current_customer')->getId();
		$collection = Mage::getModel('marketplace/userprofile')->getCollection();
        $collection->addFieldToFilter('mageuserid',array('eq'=>$partnerId));
		foreach ($collection as $record) {
			$data=$record->getPaymentsource();
		}
		return $data;
	}
	
	public function assignProduct($customer,$sid){
		$productids=explode(',',$sid);
		foreach($productids as $proid){
			$userid='';
		$product = Mage::getModel('catalog/product')->load($proid);
		if($product->getname()){   
		$collection = Mage::getModel('marketplace/product')->getCollection()->addFieldToFilter('mageproductid',array('eq'=>$proid));
		foreach($collection as $coll){
		   $userid = $coll['userid'];
		}
		if($userid){
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('The Product Is Already Assign To Other Partner.'));
		}
		else{
			$collection1 = Mage::getModel('marketplace/product');
			$collection1->setMageproductid($proid);
			$collection1->setUserid($customer->getId());
			$collection1->setStatus($product->getStatus());
			$collection1->setWebsiteIds(array(Mage::app()->getStore()->getStoreId()));
			$collection1->save();

			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('The Product Is Been Assigned To Partner.'));
			}
		} else {
			Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('adminhtml')->__("The Product Doesn't Exist."));
		} 
		}
	}
	
	public function isCustomerProducttemp($magentoProductId){
		$collection = Mage::getModel('marketplace/product')->getCollection();
		$collection->addFieldToFilter('mageproductid',array('eq'=>$magentoProductId));
		$userid='';
		foreach($collection as $record){
		$userid=$record->getuserid();
		}
		$collection1 = Mage::getModel('marketplace/userprofile')->getCollection()->addFieldToFilter('mageuserid',array('eq'=>$userid));
		foreach($collection1 as $record1){
		$status=$record1->getWantpartner();
		}
		if($status!=1){
			$userid='';
		}
		
		return array('productid'=>$magentoProductId,'userid'=>$userid);
	}

}
