<?php
Class Webkul_Marketplace_Model_Observer
{
	public function CustomerRegister($observer){
	$data=Mage::getSingleton('core/app')->getRequest();

		if($data->getParam('wantpartner')==1){

			// Add the starter message herer? 
			// 
			// 
			$customer = $observer->getCustomer();

			Mage::getSingleton('customer/session')->setIspartner(true);
			
			Mage::getModel('marketplace/userprofile')->getRegisterDetail($customer);
			$emailTemp = Mage::getModel('core/email_template')->loadDefault('partnerrequest');
			
			$emailTempVariables = array();				
			$adminEmail=Mage::getStoreConfig('trans_email/ident_general/email');
			$adminUsername = 'Admin';
			$emailTempVariables['myvar1'] = $customer->getName();
			$emailTempVariables['myvar2'] = Mage::getUrl('adminhtml/customer/edit', array('id' => $customer->getId()));
			
			$processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
			
			$emailTemp->setSenderName($customer->getName());
			$emailTemp->setSenderEmail($customer->getEmail());
			$emailTemp->send($adminEmail,$customer->getName(),$emailTempVariables);
		}
	}
	public function DeleteProduct($observer) { 
		$collection = Mage::getModel('marketplace/product')->getCollection()
														   ->addFieldToFilter('mageproductid ',$observer->getProduct()->getId());
		foreach($collection as $data){			
			Mage::getModel('marketplace/product')->load($data['index_id'])->delete();			
		}		
	}
	
	public function afterPlaceOrder($observer) { 
		$lastOrderId=$observer->getOrder()->getId();
		$order = Mage::getModel('sales/order')->load($lastOrderId);
		Mage::getModel('marketplace/saleslist')->getProductSalesCalculation($order);	
	}
	
	
	public function commissionCalculationOnComplete($observer){
	    $order = $observer->getOrder();
	    if($order->getState() == Mage_Sales_Model_Order::STATE_COMPLETE){
	    	Mage::getModel('marketplace/saleslist')->getCommsionCalculation($order);
	    }
	}
	
	public function afterSaveCustomer($observer){
		$customer=$observer->getCustomer();
		$customerid=$customer->getId();
		$isPartner= Mage::getModel('marketplace/userprofile')->isPartner();
		if($isPartner==1){
			$data=$observer->getRequest();
			$sid = $data->getParam('sellerassignproid');
			$partner_type = $data->getParam('partnertype');
			if($partner_type==2)
			{
			$collectionselectdelete = Mage::getModel('marketplace/userprofile')->getCollection();
			$collectionselectdelete->addFieldToFilter('mageuserid',array($customerid));
			foreach($collectionselectdelete as $delete){
				$autoid=$delete->getautoid();
			}
				$collectiondelete = Mage::getModel('marketplace/userprofile')->load($autoid);
				$collectiondelete->delete();
				$customer = Mage::getModel('customer/customer')->load($customerid);	
				$emailTemp = Mage::getModel('core/email_template')->loadDefault('partnerdisapprove');
			
				$emailTempVariables = array();				
				$adminEmail=Mage::getStoreConfig('trans_email/ident_general/email');
				$adminUsername = 'Admin';
				$emailTempVariables['myvar1'] = $customer->getName();
				$emailTempVariables['myvar2'] = Mage::helper('customer')->getLoginUrl();
				
				$processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
				
				$emailTemp->setSenderName($adminUsername);
				$emailTemp->setSenderEmail($adminEmail);
				$emailTemp->send($customer->getEmail(),$Username,$emailTempVariables);	
			}
			if($sid !=''||$sid!= 0){
				Mage::getModel('marketplace/userprofile')->assignProduct($customer,$sid);
			}
			$wholedata=$data->getParams();
			$collectionselect = Mage::getModel('marketplace/saleperpartner')->getCollection();
			$collectionselect->getSelect()->where('mageuserid ='.$customer->getId());
			if(count($collectionselect)==1){
			    foreach($collectionselect as $verifyrow){
				$autoid=$verifyrow->getautoid();
				}
				
				$collectionupdate = Mage::getModel('marketplace/saleperpartner')->load($autoid);
				$collectionupdate->setcommision($wholedata['commision']);
				$collectionupdate->save();
				}
			else{
				$collectioninsert=Mage::getModel('marketplace/saleperpartner');
				$collectioninsert->setmageuserid($customer->getId());
				$collectioninsert->setcommision($wholedata['commision']);
				$collectioninsert->save();
				}
		}
        else{
				$data=$observer->getRequest();
				$partner_type = $data->getParam('partnertype');
				$profileurl = $data->getParam('profileurl');
				$wholedata=$data->getParams();
				if($partner_type==1)
				{
					if($profileurl!=''){
						$profileurlcount = Mage::getModel('marketplace/userprofile')->getCollection();
						$profileurlcount->addFieldToFilter('profileurl',$profileurl);
						if(count($profileurlcount)==0){
							$collectionselect = Mage::getModel('marketplace/userprofile')->getCollection();
							$collectionselect->addFieldToFilter('mageuserid',array($customer->getId()));
							if(count($collectionselect)>=1){
								foreach($collectionselect as $coll){
										$coll->setWantpartner('1');
										$coll->setpartnerstatus('Seller');
										$coll->setProfileurl($data->getParam('profileurl'));
										$coll->save();
								}
							}	
								else{
									$collection=Mage::getModel('marketplace/userprofile');
									$collection->setwantpartner(1);
									$collection->setpartnerstatus('Seller');
									$collection->setProfileurl($data->getParam('profileurl'));
									$collection->setmageuserid($customer->getId());
									$collection->save();
							}
							$customer = Mage::getModel('customer/customer')->load($customerid);

							$emailTemp = Mage::getModel('core/email_template')->loadDefault('partnerapprove');
			
							$emailTempVariables = array();				
							$adminEmail=Mage::getStoreConfig('trans_email/ident_general/email');
							$adminUsername = 'Admin';
							$emailTempVariables['myvar1'] = $customer->getName();
							$emailTempVariables['myvar2'] = Mage::helper('customer')->getLoginUrl();
							
							$processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
							
							$emailTemp->setSenderName($adminUsername);
							$emailTemp->setSenderEmail($adminEmail);
							$emailTemp->send($customer->getEmail(),$Username,$emailTempVariables);

						} else {
							Mage::getSingleton('core/session')->addError('This Shop Name alreasy Exists.');
						}	
					}
					else{
						Mage::getSingleton('core/session')->addError('Enter Shop Name of Customer.');
					}
				}
			}
	}
	
	public function checkInvoiceSubmit($observer) { 
		$event = $observer->getEvent()->getInvoice();
		$order = Mage::getModel('sales/order')->load($event->getOrderId());
		$_collection = Mage::getModel('marketplace/saleslist')->getCollection();
		$_collection->addFieldToFilter('mageorderid',$event->getOrderId());	
		$_collection->addFieldToSelect('mageproownerid')
					->distinct(true);
		foreach($_collection as $collection){
			$fetchsale = Mage::getModel('marketplace/saleslist')->getCollection();
			$fetchsale->addFieldToFilter('mageorderid',$event->getOrderId());	
			$fetchsale->addFieldToFilter('mageproownerid',$collection->getMageproownerid());
			$totalprice ='';
			$orderinfo = '';
				$style='style="font-size:11px;padding:3px 9px;border-bottom:1px dotted #cccccc";';
				$tax="<tr><td ".$style."><h3>Tax</h3></td><td ".$style."></td><td ".$style."></td><td ".$style."></td></tr><tr>";
				$options="<tr><td ".$style."><h3>Product Options</h3></td><td ".$style."></td><td ".$style."></td><td ".$style."></td></tr><tr><td ".$style."><b>Options</b></td><td ".$style."><b>Value</b></td><td ".$style."></td><td ".$style."></td></tr>";		
			foreach($fetchsale as $res){
				$orderinfo = $orderinfo."<tr>
								<td valign='top' align='left' ".$style." >".$res['mageproname']."</td>
								<td valign='top' align='left' ".$style.">".Mage::getModel('catalog/product')->load($res['mageproid'])->getSku()."</td>
								<td valign='top' align='left' ".$style." >".$res['magequantity']."</td>
								<td valign='top' align='left' ".$style.">".Mage::app()->getStore()->formatPrice($res['mageproprice'])."</td>
							 </tr>";	
		
				foreach($order->getAllItems() as $item){
					if($item->getProductId()==$res['mageproid']){
						$taxAmount=Mage::app()->getStore()->formatPrice($item->getTaxAmount());
						$tax=$tax."<tr><td ".$style."><b>Tax Amount</b></td><td ".$style."></td><td ".$style."></td><td ".$style.">".$taxAmount."</td></tr>";
						$temp=$item->getProductOptions();
						
						if (array_key_exists('options', $temp)) {
						foreach($temp['options'] as $data){
							$optionflag=1;
							$options=$options."<tr><td ".$style."><b>".$data['label']."</b></td><td ".$style.">".$data['value']."</td><td ".$style."></td><td ".$style."></td></tr>";
							}
						 }
						else {$optionflag='';}
							
					 }
				} 
				$totalprice = $totalprice+$res['mageproprice'];
				$userdata = Mage::getModel('customer/customer')->load($res['mageproownerid']);				
				$Username = $userdata['firstname'];
				$useremail = $userdata['email'];			
			}
			
			$shipcharge = $order->getShippingAmount();
			if($item->getTaxAmount()>0){
				$orderinfo=$orderinfo.$tax;
			}
			if($optionflag==1){
				$orderinfo=$orderinfo.$options;
			}
			$orderinfo = $orderinfo."</tbody><tbody><tr>
										<td align='right' style='padding:3px 9px' colspan='3'>Grandtotal</td>
										<td align='right' style='padding:3px 9px' colspan='3'><span>".Mage::app()->getStore()->formatPrice($totalprice+$item->getTaxAmount())."</span></td>
									</tr>";
					
			$billingId = $order->getBillingAddress()->getId();
			$billaddress = Mage::getModel('sales/order_address')->load($billingId);
			$billinginfo = $billaddress['firstname'].'<br/>'.$billaddress['street'].'<br/>'.$billaddress['city'].' '.$billaddress['region'].' '.$billaddress['postcode'].'<br/>'.Mage::getModel('directory/country')->load($billaddress['country_id'])->getName().'<br/>T:'.$billaddress['telephone'];	
			
			if($order->getShippingAddress()!='')
				$shippingId = $order->getShippingAddress()->getId();
			else
				$shippingId = $billingId;
			$address = Mage::getModel('sales/order_address')->load($shippingId);				
			$shippinginfo = $address['firstname'].'<br/>'.$address['street'].'<br/>'.$address['city'].' '.$address['region'].' '.$address['postcode'].'<br/>'.Mage::getModel('directory/country')->load($address['country_id'])->getName().'<br/>T:'.$address['telephone'];	
			
			$payment = $order->getPayment()->getMethodInstance()->getTitle();
			if($order->getShippingAddress()){
				$shippingId = $order->getShippingAddress()->getId();
				$address = Mage::getModel('sales/order_address')->load($shippingId);				
				$shippinginfo = $address['firstname'].'<br/>'.$address['street'].'<br/>'.$address['city'].' '.$address['region'].' '.$address['postcode'].'<br/>'.Mage::getModel('directory/country')->load($address['country_id'])->getName().'<br/>T:'.$address['telephone'];	
				$shipping = $order->getShippingDescription();	
				$shippinfo = $shippinginfo;
				$shippingd = $shipping;		
			}
		
			$emailTemp = Mage::getModel('core/email_template')->loadDefault('webkulorderinvoice');
			
			$emailTempVariables = array();				
			$adminEmail=Mage::getStoreConfig('trans_email/ident_general/email');
			$adminUsername = 'Admin';
			$emailTempVariables['myvar1'] = $res['magerealorderid'];
			$emailTempVariables['myvar2'] = $res['cleared_at'];
			$emailTempVariables['myvar4'] = $billinginfo;
			$emailTempVariables['myvar5'] = $payment;
			$emailTempVariables['myvar6'] = $shippinfo;
			$emailTempVariables['myvar9'] = $shippingd;
			$emailTempVariables['myvar8'] = $orderinfo;
			$emailTempVariables['myvar3'] =$Username;
			
			$processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
			
			$emailTemp->setSenderName($adminUsername);
			$emailTemp->setSenderEmail($adminEmail);
			$emailTemp->send($useremail,$Username,$emailTempVariables);
		}	
	}		
}
