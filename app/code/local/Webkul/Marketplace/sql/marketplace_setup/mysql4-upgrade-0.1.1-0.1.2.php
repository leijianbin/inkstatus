<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE {$this->getTable('marketplace_product')} ADD `hitcount` int(10) NOT NULL");
$installer->run("ALTER TABLE {$this->getTable('marketplace_product')} ADD `lasthitip` varchar(255) NOT NULL");
$installer->run("ALTER TABLE {$this->getTable('marketplace_product')} ADD `lasthittime` timestamp NOT NULL");

$installer->endSetup(); 