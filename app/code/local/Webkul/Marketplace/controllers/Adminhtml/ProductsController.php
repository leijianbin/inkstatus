<?php
class Webkul_Marketplace_Adminhtml_ProductsController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('marketplace/marketplace_products')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}
	public function denyAction(){
		$wholedata=$this->getRequest()->getParams();
		$productid = $wholedata['productid'];
		$sellerid = $wholedata['sellerid'];
		$collection = Mage::getModel('marketplace/product')->getCollection()
							->addFieldToFilter('mageproductid',array('eq'=>$productid))
							->addFieldToFilter('userid',array('eq'=>$sellerid));
		foreach ($collection as $row) {
			$id = $row->getMageproductid();
			$magentoProductModel = Mage::getModel('catalog/product')->load($id);
			$catarray=$magentoProductModel->getCategoryIds();
			$categoryname='';
			$catagory_model = Mage::getModel('catalog/category');
			foreach($catarray as $keycat){
			$categoriesy = $catagory_model->load($keycat);
				if($categoryname ==''){
					$categoryname=$categoriesy->getName();
				}else{
					$categoryname=$categoryname.",".$categoriesy->getName();
				}
			}
			$allStores = Mage::app()->getStores();
			foreach ($allStores as $_eachStoreId => $val)
			{
				Mage::getModel('catalog/product_status')->updateProductStatus($id,Mage::app()->getStore($_eachStoreId)->getId(), Mage_Catalog_Model_Product_Status::STATUS_DISABLED);
			}
			$magentoProductModel->setStatus(2)->save();
			$row->setStatus(3);
			$row->save();
		}

		$subject 	= 	Mage::helper('marketplace')->__('Regarding: Product deny notification');
    	$model = Mage::getModel("mpsellerbuyercomm/sellerbuyercomm");
        $model->setProductid($productid);
        $model->setSellerid($sellerid);
        $model->setCreatedAt(time());
        $model->setCustomerid(0);
        $model->setAdmintype(1);        
        $model->setSubject($subject);
        $lastid = $model->save()->getId();

        $model = Mage::getModel("mpsellerbuyercomm/conversation");
        $model->setCommid($lastid);
        $model->setMessage($wholedata['product_deny_reason']);
        $model->setSender('admin');
        $model->setSendertype(2);
        $model->setCreatedAt(time());
        $model->save();

		$customer = Mage::getModel('customer/customer')->load($sellerid);	
		$emailTemp = Mage::getModel('core/email_template')->loadDefault('productdeny');
		$emailTempVariables = array();
		$admin_storemail = Mage::getStoreConfig('marketplace/marketplace_options/adminemail');
		$adminEmail=$admin_storemail? $admin_storemail:Mage::getStoreConfig('trans_email/ident_general/email');
		$adminUsername = 'Admin';
		$emailTempVariables['myvar1'] = $customer->getName();
		$emailTempVariables['myvar2'] = $wholedata['product_deny_reason'];
		$emailTempVariables['myvar3'] = $magentoProductModel->getName();
		$emailTempVariables['myvar4'] = $categoryname;
		$emailTempVariables['myvar5'] = $magentoProductModel->getDescription();
		$emailTempVariables['myvar6'] = $magentoProductModel->getPrice();
		
		$processedTemplate = $emailTemp->getProcessedTemplate($emailTempVariables);
		
		$emailTemp->setSenderName($adminUsername);
		$emailTemp->setSenderEmail($adminEmail);
		$emailTemp->send($customer->getEmail(),$adminUsername,$emailTempVariables);
		$sellername = $customer->getName();
		$this->_getSession()->addSuccess($magentoProductModel->getName().Mage::helper('marketplace')->__(' has been successfully denied'));

		$this->_redirect('marketplace/adminhtml_products/');
	}
	public function approveAction(){
		$id = (int)$this->getRequest()->getParam('id');

		$wholedata=$this->getRequest()->getParams();
		$id = $wholedata['productid_approve'];
		$sellerid = $wholedata['sellerid_approve'];
		$product_reason = $wholedata['product_reason'];

		if(!$id){$this->_redirectReferer();}
		$name=Mage::getModel('marketplace/product')->approveSimpleProduct($id,$product_reason);
		Mage::getSingleton('adminhtml/session')->addSuccess($name.Mage::helper('marketplace')->__(' has been successfully approved.'));
		$this->_redirect('marketplace/adminhtml_products/');
		//$this->_redirect('adminhtml/catalog_product/edit', array('id' => $lastId,'_current'=>true));
		
	}
	public function massapproveAction(){
		$ids = $this->getRequest()->getParam('marketplaceproduct');
        if(!is_array($ids)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($ids as $id) {
                	$sellerproduct = Mage::getModel('marketplace/product')->load($id);
					$lastId=Mage::getModel('marketplace/product')->approveSimpleProduct($sellerproduct->getMageproductid());
				}
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully aprroved', count($ids)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
		}
		$this->_redirect('*/*/index');
	}
	public function gridAction(){
            $this->loadLayout();
            $this->getResponse()->setBody($this->getLayout()->createBlock('marketplace/adminhtml_products_grid')->toHtml()); 
        }
}
