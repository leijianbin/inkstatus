<?php
class Webkul_Marketplace_SellerController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
		$this->loadLayout();     
		$this->renderLayout();
    }
	public function profileAction(){
		$this->loadLayout();     
		$this->renderLayout();
	}
	public function collectionAction(){
		$this->loadLayout();     
		$this->renderLayout();
	}
	public function locationAction(){
		$this->loadLayout();     
		$this->renderLayout();
	}
	public function feedbackAction(){
		$this->loadLayout();     
		$this->renderLayout();
	}
	public function usernameverifyAction(){
		$profileurl=$this->getRequest()->getParam('profileurl');
		$collection=Mage::getModel('marketplace/userprofile')->getCollection()
							->addFieldToFilter('profileurl',array('eq'=>$profileurl));
		echo count($collection);
	}
	public function sendmailAction(){		
		$data = $this->getRequest()->getParams();
		Mage::dispatchEvent('mp_send_querymail', $data);

		$emailTemplate = Mage::getModel('core/email_template')->loadDefault('querypartner_email');
		$emailTemplateVariables = array();
		$mail=Mage::getModel('customer/customer')->load($data['seller-id']);
		$emailTemplateVariables['myvar1'] =$mail->getName();
		$sellerEmail = $mail->getEmail();
		$emailTemplateVariables['myvar3'] =Mage::getModel('catalog/product')->load($data['product-id'])->getName();
		$emailTemplateVariables['myvar4'] =$data['ask'];
		$emailTemplateVariables['myvar5'] =$data['email'];
		$myname =Mage::getSingleton('customer/session')->getCustomer()->getName() ;
		if(strlen($myname)<2){$myname="Guest";}
		$processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
		$emailTemplate->setSenderName($myname);
		$emailTemplate->setSenderEmail($data['email']);
		$emailTemplate->send($sellerEmail,$myname,$emailTemplateVariables);
		echo json_encode("true");
    }
	public function newfeedbackAction(){
		 if (!$this->_validateFormKey()) {
           return $this->_redirect('marketplace/marketplaceaccount/myproductslist/');
         }
		$wholedata=$this->getRequest()->getPost();
		Mage::getModel('marketplace/feedback')->saveFeedbackdetail($wholedata);
		Mage::getSingleton('core/session')->addSuccess('Your Review was successfully saved');
		$this->_redirect("marketplace/seller/feedback/".$wholedata['profileurl'].'/.');
		
	}
}
